import cv2
import numpy as np
import scipy.misc
import os
import sys
from components import *
from params import *
from utils import *
import panel.client
import subprocess
import osc.sound
import trigger

from stopper import Stopper

import time

import mathutils as u

from weather import weather


class Store:
    pass

store = Store()


rainy_classes = [
    ClearLayer,
    Rainy1AKirisame,
    Rainy1BNamidaame,
    Rainy1CSou,
    Rainy2AOshimeri,
    Rainy2BKannu,
    Rainy2CKaiu1,
    Rainy3AShinotsukuame,
    Rainy3BTeppouame,
    Rainy3CKiu,
]

background_classes = {
    "cloudy": CloudyBackground2,
    "rainy": RainyBackground2,
    "sunny": SunnyBackground,
}


class Prev:
    def __init__(self):
        self.object = None
        self.background = None
        self.rain = 0
        self.weather = None

class Main():
    def __init__(self):
        store.main = self
        self.prev = Prev()
        self.recording = False
        self.scale = 66
        self.aspect = 2380 / 1150 # W / H
        self.pixel = np.ones((HEIGHT, WIDTH, 3))
        h, w, _ = self.pixel.shape
        self.window_size = (int(h * self.scale / self.aspect), int(w * self.scale))
        self.grid = self.makeGrid()
        self.final_shape = None
        self.max_mode = '--max' in sys.argv

        # layers
        self.background = RainyBackground2()
        self.prev.background = None
        self.prev.background_timer = time.time()
        self.special = None

        # send data to sound
        self.send_sound_mode_timer = time.time()

        params.current_class = ClearLayer

        self.index = 0
        self.object_class = params.current_class
        self.object = self.object_class()
        self.prev.object = None

        # self.test_object = CloudyBackground2()
        self.test_object = None

        # stopping function
        # disabled if debug
        self.stopper = Stopper()

        # ui
        self.beginServer()
        panel.client.open()

        # start sound anyway
        osc.sound.client.send_start()
        self.loop()

    def update(self):
        self.stopper.update()

        weather.update()
        to_send_to_sound = False
        # 何かしら変わっていたら
        if weather.weather != self.prev.weather or weather.rain != self.prev.rain:
            # 一旦こっちに反映。
            print('rain detected!!', weather.rain)
            index = weather.rain
            index = min(len(rainy_classes) - 1, weather.rain)
            params.current_class = rainy_classes[index]
            to_send_to_sound = True

        if weather.weather != self.prev.weather:
            self.prev.background = self.background
            self.background = background_classes[weather.weather]()
            self.prev.background_timer = time.time()
            to_send_to_sound = True

            # スペシャル演出
            if weather.weather == 'rainy' and self.prev.weather != 'rainy':
                self.special = BeginRainning()
                self.send_special_to_sound()
            if weather.weather != 'rainy' and self.prev.weather == 'rainy':
                self.special = EndRainning()
                self.send_special_to_sound()

        if weather.special:
            if weather.special == 'thunder' and self.special.__class__ is not Thunder:
                self.special = Thunder()
                weather.special = None
                to_send_to_sound = True
                self.send_special_to_sound()

            if weather.special == 'kitsune' and self.special.__class__ is not Kitsune:
                self.special = Kitsune()
                weather.special = None
                to_send_to_sound = True

        if self.object_class is not params.current_class:
            # 前の演出もしばらく見せる
            self.prev.object = self.object
            self.object_class = params.current_class
            self.object = self.object_class()

        trigger.trigger.update()
        self.background.update()
        if self.prev.background:
            self.prev.background.update()
        self.object.update()

        if self.special:
            self.special.update()
            if not self.special.is_active:
                self.special = None

        if self.test_object:
            self.test_object.update()

        trigger.trigger.flush()

        if self.prev.object:
            self.prev.object.update()

        trigger.trigger.beat.flush()

        if self.stopper.to_start:
            osc.sound.client.send_start()

        if self.stopper.to_stop:
            osc.sound.client.send_stop()

        if to_send_to_sound or time.time() - self.send_sound_mode_timer > 1.0:
            self.send_mode_to_sound()

        self.prev.weather = weather.weather
        self.prev.rain = weather.rain

    def send_special_to_sound(self):
        if not self.stopper.is_playing:
            return
        klass = self.special.__class__
        if klass is BeginRainning:
            special = 'rain_begin'
        elif klass is EndRainning:
            special = 'rain_end'
        elif klass is Thunder:
            special = 'thunder'
        elif klass is Kitsune:
            special = 'kitsune'
        else:
            special = 'none'

        osc.sound.client.send_special(special)

    def send_mode_to_sound(self):
        if not self.stopper.is_playing:
            return
        mode = weather.weather

        if mode == 'rainy':
            mode += "_{}".format(weather.rain)

        if self.special and self.special.__class__ is Kitsune:
            mode = 'kitsune'

        osc.sound.client.send_mode(mode)

        self.send_sound_mode_timer = time.time()

    def draw(self):
        if not self.stopper.is_playing:
            self.pixel = np.zeros((HEIGHT, WIDTH, 3)).astype(float)
            return

        pixel = self.background.draw()
        if self.prev.background:
            alpha = u.smoothstep(time.time(), self.prev.background_timer + 3, 3)
            alpha = np.ones((1, 1, 1)) * alpha
            pixel = alpha_brend(self.prev.background.draw(), pixel, alpha)
            if alpha == 1:
                self.prev.background = None

        if self.prev.object:
            layer = self.prev.object.draw()
            if layer.shape[2] == 4: # Go Alpha Brending
                alpha = layer[:, :, 3]
                rgb = layer[:, :, :3]
                pixel = add_brend(pixel, rgb, alpha)
            else:
                pixel = layer

            if layer.sum() == 0:
                self.prev.object = None

        layer = self.object.draw()
        if layer.shape[2] == 4: # Go Alpha Brending
            alpha = layer[:, :, 3]
            rgb = layer[:, :, :3]
            pixel = add_brend(pixel, rgb, alpha)
        else:
            pixel = layer

        if self.special:
            layer = self.special.draw()
            if layer.shape[2] == 4: # Go Alpha Brending
                alpha = layer[:, :, 3]
                rgb = layer[:, :, :3]
                pixel = alpha_brend(pixel, rgb, alpha)
            else:
                pixel = layer

        # デバッグ用のレイヤー
        if self.test_object:
            layer = self.test_object.draw()
            if layer.shape[2] == 4: # Go Alpha Brending
                alpha = layer[:, :, 3]
                rgb = layer[:, :, :3]
                pixel = alpha_brend(pixel, rgb, alpha)
            else:
                pixel = layer

        self.pixel = pixel

    def beginServer(self):
        self.server = subprocess.Popen('cd panel; python ui.py', shell=True)
        self.hub = subprocess.Popen('cd panel; python hub.py', shell=True)

    def loop(self):
        print("click 'RAIN' window and hit return key to change animation... ")
        while(True):
            self.update()
            self.draw()

            h, w, _ = self.pixel.shape
            scale = self.scale
            aspect = self.aspect
            image = (self.pixel * 255).astype(np.uint8)
            if self.max_mode:
                image = scipy.misc.imresize(image, self.window_size, interp='nearest')
            else:
                image = scipy.misc.imresize(image, self.window_size, interp='nearest')
                image *= self.grid
                image = self.addBigGrid(image)
            if self.final_shape is None: 
                self.final_shape = image.shape
            cv2.imshow("RAIN", image)
            cv2.moveWindow("RAIN", 0, 0)

            if self.recording:
                self.video.write(image)

            charcode = cv2.waitKey(33)
            if charcode > 0:
                char = chr(charcode)
                if char == 'q':
                    return
                if char == 'r':
                    if self.recording:
                        print('finish recording')
                        self.finishRec()
                    else:
                        print('start recording')
                        self.startRec()

                if charcode == 13: # enter
                    self.index += 1
                    if self.index >= len(self.objects):
                        self.index = 0
                    self.object = self.objects[self.index]

    def addBigGrid(self, frame):
        thickness = 3
        h, w, d = frame.shape
        image = np.zeros((h + thickness, w, d), dtype=frame.dtype)
        y = int(h / 4 * 3)
        image[:y] = frame[:y]
        image[y+thickness:] = frame[y:]
        return image

    def makeGrid(self):
        thickness = 2
        image = np.ones(self.window_size, dtype=np.uint8)
        h, w = image.shape
        h_ = h - thickness * 2
        w_ = w - thickness
        for i in range(HEIGHT + 1):
            y = int(thickness / 2 + h_/HEIGHT * i)
            image = cv2.line(image, (0, y), (w, y), (0, 0, 0), thickness)
        for i in range(WIDTH + 1):
            x = int(thickness / 2 + w_/WIDTH * i)
            image = cv2.line(image, (x, 0), (x, h), (0, 0, 0), thickness)
        image = image[:, :, None]
        return image

    def startRec(self):
        os.system('rm output/video.mov')
        fourcc = cv2.VideoWriter_fourcc('m','p','4','v')
        self.video = cv2.VideoWriter('output/video.mov', fourcc, 30.0, self.final_shape[:2][::-1])
        self.recording = True

    def finishRec(self):
        self.video.release()
        self.video = None
        self.recording = False


try:
    Main()
except:
    import traceback
    traceback.print_exc()
finally:
    # os.system('./panel/kill.sh')
    store.main.server.terminate()
    store.main.hub.terminate()
    osc.sound.server.close()
