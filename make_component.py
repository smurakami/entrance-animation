import re
import os
import sys


def snail2camel(s):
    return re.sub(r'(^|_)[a-z0-9]', lambda x: x.group(0).upper().replace('_', ''), s)


if len(sys.argv) < 3:
    print('usage: python make_component.py baseclass name')

_, baseclass, name = sys.argv


dirname, baseclass = os.path.split(baseclass)
name = os.path.basename(name)

# os.system('cp component/{} component/{}'.format(baseclass, name))

org_classname = baseclass.replace('.py', '')
org_classname = snail2camel(org_classname)

classname = name.replace('.py', '')
classname = snail2camel(classname)

output = 'components/{}'.format(name)

if os.path.exists(output):
    print('already exists!')
    exit()

with open(output, 'w') as f:
    for line in open(os.path.join(dirname, baseclass)):
        line = line.replace('\n', '')
        line = line.replace(org_classname, classname)
        print(line, file=f)
