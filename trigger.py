import osc
import numpy as np
from params import params
import time
import argparse

import sys
from args import args


class Trigger:
    def __init__(self):
        self.buffer = []
        self.debug = args.debug
        self.beat = Beat()
        self.limitter = Limitter()

    def update(self):
        self.beat.update()
        data = []
        if self.debug:
            num = int(params.common.get_freq() / np.random.random())
            if num > 0:
                data = [True]
        else:
            if len(osc.sound.server.buffer) > 0:
                data = osc.sound.server.flush()

        count = self.limitter.update_and_check(len(data))
        self.buffer += data[:count]

    def check(self):
        return len(self.buffer)

    def flush(self):
        data = self.buffer
        self.buffer = []
        return len(data)

    def pop(self):
        if len(self.buffer) > 0:
            self.buffer.pop()
            return 1
        else:
            return 0


class Limitter:
    def __init__(self):
        self.buffer = []

    def update_and_check(self, count):
        limit = params.common.get_freq_limit()
        # limitが0のときは、ノーリミット
        if limit == 0:
            return count

        time_ = time.time()
        # 一秒以内に発生した履歴を保存。
        self.buffer = [start for start in self.buffer if self.buffer if time_ - start <= 1]

        counter = 0
        for i in range(count):
            if len(self.buffer) > limit:
                break
            self.buffer.append(time_)
            counter += 1

        return counter


class Beat:
    def __init__(self):
        self.buffer = []
        self.debug = args.debug
        self.before = time.time()

    def update(self):
        if self.debug:
            beat = params.common.get_beat()
            if time.time() - self.before >= beat:
                self.buffer.append(True)
                self.before = time.time()
        else:
            if len(osc.sound.server.beat.buffer) > 0:
                data = osc.sound.server.beat.flush()
                data = [(addr, val) for addr, val in data if val % 8 == 0]
                self.buffer += data

    def flush(self):
        data = self.buffer
        self.buffer = []
        return len(data)
        
    def check(self):
        return len(self.buffer)

    def pop(self):
        if len(self.buffer) > 0:
            self.buffer.pop()
            return 1
        else:
            return 0


trigger = Trigger()
