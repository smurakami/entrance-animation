import numpy as np

def __smoothstep_zero(x, smooth):
    x = np.array(x)
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5

    if len(x.shape) == 0:
        if x < -smooth:
            return 0
        elif x > smooth:
            return 1
        else:
            return y
    else:
        y[x < -smooth] = 0
        y[x > smooth] = 1
        return y

def smoothstep(x, th, smooth):
    if smooth == 0:
        step(x, th)
    else:
        return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

