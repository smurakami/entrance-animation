import argparse

def init():
    parser = argparse.ArgumentParser()
    parser.add_argument("--test", action='store_true')
    parser.add_argument("--debug", action='store_true')
    parser.add_argument("--zententou", action='store_true')
    args = parser.parse_args()
    return args

args = init()
