"""Small example OSC client
This program sends 10 random values between 0.0 and 1.0 to the /filter address,
waiting for 1 seconds between each value.
"""
import argparse
import random
import time

from pythonosc import osc_message_builder
from pythonosc import udp_client

import subprocess
import re

import socket


def get_ip_address():
    out = subprocess.Popen('ifconfig | grep "inet "', stdout=subprocess.PIPE, shell=True).communicate()
    lines = out[0].decode('utf-8').split('\n')

    info = [line for line in lines if 'broadcast' in line]

    if len(info) == 0:
        return '127.0.0.1'

    info = info[0]

    address, broadcast = re.findall(r'\d+.\d+.\d+.\d+', info)
    return address, broadcast


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  my_ip, broadcast = get_ip_address()

  port = 54325
  client = udp_client.SimpleUDPClient(my_ip, port)
  client._sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # not working...

  print(client)

  for x in range(10):
    print('send')
    client.send_message("/sound", random.random())
    time.sleep(1)
    