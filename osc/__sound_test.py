"""Small example OSC server
This program listens to several addresses, and prints some information about
received packets.
"""
import math
import socket

from pythonosc import dispatcher
from pythonosc import osc_server


addresses = [
    "/mode", 
    "/pulse", 
    "/move"
]


if __name__ == "__main__":
    my_ip = "192.168.1.169"
    port = 54324
  
    dispatcher = dispatcher.Dispatcher()

    for addr in addresses:
        dispatcher.map(addr, print)

    server = osc_server.ThreadingOSCUDPServer(
        (my_ip, port), dispatcher)
  
    print("Serving on {}".format(server.server_address))
    server.serve_forever()




