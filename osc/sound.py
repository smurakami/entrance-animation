"""Small example OSC client
This program sends 10 random values between 0.0 and 1.0 to the /filter address,
waiting for 1 seconds between each value.
"""
import random
import time

from pythonosc import osc_message_builder
from pythonosc import udp_client
from pythonosc import dispatcher
from pythonosc import osc_server
import socket

import numpy as np
import threading
# import argparse
import sys
sys.path.append('..')
from args import args

import socket

import subprocess
import re


def get_ip_address():
    out = subprocess.Popen('ifconfig | grep "inet "', stdout=subprocess.PIPE, shell=True).communicate()
    lines = out[0].decode('utf-8').split('\n')

    info = [line for line in lines if 'broadcast' in line]

    if len(info) == 0:
        return '127.0.0.1', '127.0.0.1'

    info = info[0]

    address, broadcast = re.findall(r'\d+.\d+.\d+.\d+', info)
    return address, broadcast


# sound_ip = '169.254.177.45'
sound_ip = '10.7.224.202'
# sound_ip = '127.0.0.1'
# my_ip = "192.168.1.169"
# my_ip = "169.254.205.245"
# my_ip = "127.0.0.1"
my_ip, broadcast = get_ip_address()

class SoundClient:
    def __init__(self):
        port = 54324
        test = False

        if test:
            self.client = udp_client.SimpleUDPClient(my_ip, port)
        else:
            self.client = udp_client.SimpleUDPClient(sound_ip, port)

        self.client._sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # not working...

    def send_message(self, address, message):
        try:
            self.client.send_message(address, message)
        except:
            print('sound client error!!!', address, message)

    def send_mode(self, mode):
        # print('send mode message', mode)
        self.send_message("/mode", mode)

    def send_start(self):
        # print('send mode message', mode)
        self.send_message("/master/start", 0)

    def send_stop(self):
        # print('send mode message', mode)
        self.send_message("/master/stop", 0)

    def send_special(self, special):
        self.send_message("/special", special)

    def send_pulse(self, pos):
        # パルス系のエフェクトで使う。
        self.send_message("/pulse", pos)

    def send_move(self, pos):
        self.send_message("/move", pos)


client = SoundClient()


class SoundServer:
    def __init__(self):
        self.buffer = []
        port = 54325
        self.counter = 0

        self.beat = Beat()

        self.dispatcher = dispatcher.Dispatcher()
        self.dispatcher.map("/sound", self.callback)
        self.dispatcher.map("/tick", self.callback)
      
        self.server = osc_server.ThreadingOSCUDPServer(
            (my_ip, port), self.dispatcher)
        self.server.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # not working...
        self.open()
        # self.server.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        # self.server.serve_forever()

    def callback(self, address, data):
        # print(address, data)
        if address == '/sound':
            self.buffer.append((address, data))
        elif address == '/tick':
            self.beat.buffer.append((address, data))

    def flush(self):
        data = self.buffer
        self.buffer = []
        return data

    def open(self):
        self.thread = threading.Thread(target = self.server.serve_forever)
        self.thread.start()
        print("Serving on {}".format(self.server.server_address))

    def close(self):
        self.server.shutdown()

class Beat:
    def __init__(self):
        self.buffer = []

    def flush(self):
        data = self.buffer
        self.buffer = []
        return data

server = SoundServer()

if __name__ == "__main__":

    try:
        while True:
            time.sleep(1)
    except:
        import traceback
        traceback.print_exc()
    finally:
        server.close()

    # pass

    # server = SoundServer()
    # sound = Sound()
    # for i in range(10):
    #     sound.send_pulse(np.array([2, 5]).tolist())
    #     time.sleep(1)

        
