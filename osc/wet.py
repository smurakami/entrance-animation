"""Small example OSC server
This program listens to several addresses, and prints some information about
received packets.
"""
import math
import socket

from pythonosc import dispatcher
from pythonosc import osc_server

import numpy as np

import re

import subprocess
import threading

from datetime import datetime


# addresses = [
#     '/time',
#     '/trend',
#     '/barometer',
#     '/in_temp',
#     '/in_hum',
#     '/out_temp',
#     '/out_hum',
#     '/wind_speed',
#     '/ten_min_avg_windspeed',
#     '/wind_direction',
#     '/rain_rate',
#     '/uv',
#     '/solar_radiation',
#     '/forecast_icons',
#     '/rain_drop',
#     '/bass_atack',
# ]


def get_ip_address():
    out = subprocess.Popen('ifconfig | grep "inet "', stdout=subprocess.PIPE, shell=True).communicate()
    lines = out[0].decode('utf-8').split('\n')

    info = [line for line in lines if 'broadcast' in line]

    if len(info) == 0:
        return '127.0.0.1', '127.0.0.1'

    info = info[0]

    address, broadcast = re.findall(r'\d+.\d+.\d+.\d+', info)
    return address, broadcast


my_ip, broadcast = get_ip_address()

class WetServer:
    def __init__(self):
        self.buffer = []
        port = 60003
        self.init_params()
        self.wet_buffer = []

        self.dispatcher = dispatcher.Dispatcher()
        self.dispatcher.map('/wet', self.callback)
      
        self.server = osc_server.ThreadingOSCUDPServer(
            (my_ip, port), self.dispatcher)
        self.server.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # not working...
        self.open()

    def init_params(self):
        self.wet = 0

    def callback(self, address, data):
        print(datetime.now(), address, data)
        print(datetime.now(), address, data, file=open('log/log.log', 'a'))
        if address == '/wet':
            self.wet = data
            self.wet_buffer.append(data)
            buffer_len = 30
            if len(self.wet_buffer) > buffer_len:
                self.wet_buffer = self.wet_buffer[buffer_len:]

    def mean_wet(self):
        if len(self.wet_buffer) == 0:
            return 0
        else:
            return np.mean(self.wet_buffer)

    def open(self):
        self.thread = threading.Thread(target = self.server.serve_forever)
        self.thread.start()
        print("Serving on {}".format(self.server.server_address))

    def close(self):
        self.server.shutdown()


server = WetServer()


if __name__ == "__main__":
    pass
    # my_ip = "192.168.1.169"
    # port = 54323
  
    # dispatcher = dispatcher.Dispatcher()

    # for addr in addresses:
    #     dispatcher.map(addr, print)

    # server = osc_server.ThreadingOSCUDPServer(
    #     (my_ip, port), dispatcher)
  
    # print("Serving on {}".format(server.server_address))
    # server.serve_forever()




