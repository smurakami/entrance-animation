import glob
import re
import os
import json


def snail2camel(s):
    return re.sub(r'(^|_)[a-z0-9]', lambda x: x.group(0).upper().replace('_', ''), s)

params = {}

for file in glob.glob('components/*.py'):
    if file == '__init__':
        continue

    name = os.path.basename(file)
    name = name.replace('.py', '')
    name = os.path.basename(name)

    param = {}

    for line in open(file):
        match = re.search(r'# !param (.*)', line);
        if match:
            s = match.group(1)
            s = re.sub(r'(\w+):', r'"\1":', s)
            s = re.sub(r':(\w+)', r'"\1"', s)
            s = "{" + s + "}"
            data = json.loads(s)

            key = data['key']
            del data['key']
            param[key] = data

    params[name] = param

print(params)

