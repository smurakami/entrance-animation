from bottle import route, run, template, static_file, response, request, TEMPLATE_PATH, default_app

import bottle
import json
import os
import requests
import sys
import glob
import re

import sys
sys.path.append('..')

import params as P

TEMPLATE_PATH.insert(0, os.path.abspath(os.path.dirname(__file__)))

isLocal = '--local' in sys.argv


@route('/')
def index():
    return template('ui/index')

@route('/<filepath:path>')
def assets(filepath):
    return static_file(filepath, root="ui")

@route('/params', method='get')
def params():
    return {
        'common': P.load_common_params(),
        'params': P.load_params(cd='../')
    }


if __name__ == '__main__':
    run(host='0.0.0.0', port=8888, debug=True)
else:
    application = default_app()


