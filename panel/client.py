import sys
sys.path.append('..')
import websocket
import _thread as thread
import time
import logging
import json
from concurrent.futures import ThreadPoolExecutor
import params
from components import *

from weather import weather


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(' %(module)s -  %(asctime)s - %(levelname)s - %(message)s'))
logger.addHandler(handler)


class Socket:
    def __init__(self):
        self.active = True
        self.ws = None

    def close(self):
        self.active = False
        self.ws.close()

    def open(self):
        websocket.enableTrace(False)
        ws = websocket.WebSocketApp("ws://127.0.0.1:12345",
                                  on_message = self.on_message,
                                  on_error = self.on_error,
                                  on_close = self.on_close)
        self.ws = ws
        ws.on_open = self.on_open
        def run(*args):
            # logger.info('open')
            ws.run_forever()

        thread.start_new_thread(run, ())

    def on_message(self, ws, message):
        # logger.info('Received:{}'.format(message))
        data = json.loads(message)

        if 'is_weather' in data:
            print(data)
            weather.weather = data['weather']
            return

        if 'is_rain' in data:
            weather.rain = data['rain']
            print(data)
            return

        if 'is_special' in data:
            weather.special = data['special']
            print(data)
            return

        if 'is_wind_speed' in data:
            weather.wind = data['val']
            return

        if 'dump' in data:
            params.params.dump()
            return

        if 'change_class' in data:
            params.params.current_class = eval(data['classname'])
            return

        if 'is_common' in data:
            params.params.common.set(data)
            return

        classname = data['classname']
        name = data['name']
        val = data['val']
        params.params.set(classname, name, val)
     
    def on_error(self, ws, error):
        pass
        # logger.info('Error:{}'.format(error))
     
    def on_close(self, ws):
        # logger.info('Close')
        if self.active:
            time.sleep(1)
            self.open()

    def on_open(self, ws):
        data = {
            "is_generator": True
        }
        ws.send(json.dumps(data))
        # logger.info('Sent:{}'.format(data))

socket = Socket()

def open():
    socket.open()


def close():
    socket.close()
 
if __name__ == "__main__":
    open()
    for i in range(3):
        time.sleep(1)
        print(params.params.data)
    close()
