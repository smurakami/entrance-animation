import logging
import json
from websocket_server import WebsocketServer
 
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter(' %(module)s -  %(asctime)s - %(levelname)s - %(message)s'))
logger.addHandler(handler)
 
# Callback functions

class Main:
    def __init__(self):
        self.generator = None

main = Main()
 
def new_client(client, server):
    pass
    # logger.info('New client {}:{} has joined.'.format(client['address'][0], client['address'][1]))
 
def client_left(client, server):
    # logger.info('Client {}:{} has left.'.format(client['address'][0], client['address'][1]))
    if client == main.generator:
        print('generator has left.')
        main.generator = None
 
def message_received(client, server, message):
    data = json.loads(message)
    # print(data)
    if 'is_generator' in data:
        main.generator = client
        return

    if main.generator:
        server.send_message(main.generator, message)
 
# Main
if __name__ == "__main__":
    server = WebsocketServer(port=12345, host='127.0.0.1', loglevel=logging.INFO)
    server.set_fn_new_client(new_client)
    server.set_fn_client_left(client_left)
    server.set_fn_message_received(message_received)
    server.run_forever()
  
