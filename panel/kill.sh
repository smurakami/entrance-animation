#!/bin/bash

lsof -i:8888 | grep python | awk '{print $2}' | xargs kill -9          
lsof -i:12345 | grep python | awk '{print $2}' | xargs kill -9          
