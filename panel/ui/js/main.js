var Socket = function() {
  this.init();
};

Socket.prototype = {
  init: function() {
    this.open();
  },
  open: function() {
    this.s = new WebSocket('ws://127.0.0.1:12345');
    this.s.onopen = () => {
      console.log('connected');
    };
    this.s.onmessage = () => {
      console.log('message');
    };
    this.s.onerror = () => {
      console.log('error');
    };
    this.s.onclose = () => {
      console.log('closed');
      setTimeout(() => {
        this.open();
      }, 1000);
    };
  },
  send: function(data) {
    this.s.send(JSON.stringify(data));
  },
};


var Main = function(){
  this.init();
};


Main.prototype = {
  init: function(){
    this.socket = new Socket();
    this.initWeatherButtons();
    $.ajax({url: '/params', dataType: 'json'}).then((data) => {
      this.initCommonParams(data['common'])
      this.initParams(data['params']);
      $('.dump_button .btn').click( () => {
        this.socket.send({
          dump: true
        })
      });
    }).fail((jqXHR, textStatus, errorThrown) => {
      console.log('fail');
      console.log(jqXHR.status);
      console.log(textStatus);
      console.log(errorThrown);
    });
  },

  initWeatherButtons() {
    var self = this;
    $('.weather .kind .btn').click(function() {
      weather = $(this).text();
      self.socket.send({
        is_weather: true,
        weather: weather
      });
    });
    $('.weather .rain .btn').click(function() {
      rain = Number($(this).text());
      self.socket.send({
        is_rain: true,
        rain: rain
      });
    });
    $('.weather .special .btn').click(function() {
      special = $(this).text();
      self.socket.send({
        is_special: true,
        special: special
      });
    });

    $('.weather .wind_speed input').on('input', function(){
      var val = Number($(this).val());
      self.socket.send({
        is_wind_speed: true,
        val: val
      });
    });
  },

  initCommonParams(data) {
    var self = this;
    var classbody_template = $('.common_params .classbody');
    classbody_template.remove();
    // var classname_template = $('.classname');
    // // classname_template.remove();

    for (var classname in data) {
      // $('.params').append('<h2>' + classname + '</h2>');
      var classbody = classbody_template.clone();
      classbody.find('.title').text(classname);
      classbody.find('.button').data('classname', classname);
      classbody.find('.button').click(function() {
        var classname = $(this).data('classname');
        self.socket.send({
          change_class: true,
          classname: classname,
        });
      });

      var template = classbody.find('.param');
      template.remove();

      params = data[classname];
      for (var name in params) {
        param = params[name];
        var node = template.clone();
        node.find('.title').text(classname + '.' + name);
        var input = node.find('input');
        input.data('name', name)
        input.data('classname', classname)
        input.attr('min', param.min);
        input.attr('max', param.max);
        input.attr('value', param.value);
        input.on('input', function(){
          var val = Number($(this).val());
          var classname = $(this).data('classname');
          var name = $(this).data('name');
          self.socket.send({
            // classname: classname,
            is_common: true,
            name: name,
            val: val,
          });
        });
        input.change(function() {
          var val = Number($(this).val());
          console.log(val);
        })
        classbody.append(node);
      }
      $('.common_params').append(classbody);
    }
  },

  addParam: function(classname, name, param) {
    var self = this;
    var node = this.template.clone();
    node.find('.title').text(classname + '.' + name);
    var input = node.find('input');
    input.data('name', name)
    input.data('classname', classname)
    input.attr('min', param.min);
    input.attr('max', param.max);
    input.attr('value', param.value);
    input.on('input', function(){
      var val = Number($(this).val());
      var classname = $(this).data('classname');
      var name = $(this).data('name');
      self.socket.send({
        classname: classname,
        name: name,
        val: val,
      });
    });
    input.change(function() {
      var val = Number($(this).val());
      console.log(val);
    });
    return node;
  },

  addColorParam: function(classname, name, param) {
    var self = this;
    var node = this.template.clone();
    node.find('.title').text(classname + '.' + name);
    var input_ = node.find('input');
    input_.remove();

    var value = param.value;

    var ps = ['h', 's', 'v'];
    for (var i = 0; i < 3; i++) {
      var input = input_.clone()
      p = ps[i];
      input.data('name', name);
      input.data('classname', classname);
      input.attr('min', 0);
      input.attr('max', p == 'h' ? 360 : 1);
      input.attr('value', param.value[i]);

      (() => {
        var i_ = i;
        input.on('input', function(){
          var val = Number($(this).val());
          var classname = $(this).data('classname');
          var name = $(this).data('name');
          value[i_] = val;
          self.socket.send({
            classname: classname,
            name: name,
            val: value,
          });
        });
        input.change(function() {
          console.log(value);
        });
      })();
      node.append(input);
    }

    return node;
  },

  initParams: function(data) {
    var self = this;
    var classbody_template = $('.params .classbody');
    classbody_template.remove();
    // var classname_template = $('.classname');
    // // classname_template.remove();

    for (var classname in data) {
      // $('.params').append('<h2>' + classname + '</h2>');
      var classbody = classbody_template.clone();
      classbody.find('.title').text(classname);
      classbody.find('.button').data('classname', classname);
      classbody.find('.button').click(function() {
        var classname = $(this).data('classname');
        self.socket.send({
          change_class: true,
          classname: classname,
        });
      });

      var template = classbody.find('.param');
      template.remove();
      this.template = template;

      params = data[classname];
      for (var name in params) {
        param = params[name];

        if (param.is_color == true) {
          var node = this.addColorParam(classname, name, param);
        } else {
          var node = this.addParam(classname, name, param);
        }
        classbody.append(node);
      }
      $('.params').append(classbody);
    }
  },
}


$(document).ready(function(){
  window.main = new Main()
})
