# エントランス照明の検証プログラムです。

## Requirements

```
Python3
opencv: 画像の読み書き
numpy: シミュレーション
scipy: 画像のリサイズ

```

## Usage

```
python main.py
```

* "RAIN"ウインドウをクリックして、エンターキーでアニメーションを切り替えられます。
* "R"キーで録画の開始/停止が可能です。

