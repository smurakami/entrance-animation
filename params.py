import glob
import os
import re
import json
import cv2
import numpy as np
import time

WIDTH = 21
HEIGHT = 4

class Params:
    def __init__(self):
        self.data = load_params()
        self.current_class = None
        self.common = Common()

    def fget(self, filename, name):
        def snail2camel(s):
            return re.sub(r'(^|_)[a-z0-9]', lambda x: x.group(0).upper().replace('_', ''), s)
        classname = os.path.basename(filename)
        classname = classname.replace('.py', '')
        classname = os.path.basename(classname)
        classname = snail2camel(classname)
        return self.get(classname, name)

    def get(self, classname, name):
        if classname in self.data:
            class_ = self.data[classname]
            if name in class_:
                p = class_[name]
                return p['value']
        return None

    def set(self, classname, name, value):
        # if not classname in self.data:
        #     self.data[classname] = {}
        # if not name in self.data[classname]:
        #     self.data[classname][name] = {}
        self.data[classname][name]['value'] = value

    def dump(self):
        with open('output/{}.json'.format(int(time.time())), 'w') as f:
            json.dump({
                'common': self.common.data,
                'params': self.data
                }, f, indent=4)



def load_params(cd='./'):
    def snail2camel(s):
        return re.sub(r'(^|_)[a-z0-9]', lambda x: x.group(0).upper().replace('_', ''), s)

    params = {}
    for file in sorted(glob.glob(os.path.join(cd, 'components/*.py'))):

        name = os.path.basename(file)
        name = name.replace('.py', '')
        name = os.path.basename(name)
        if name == '__init__':
            continue
        name = snail2camel(name)

        param = {}

        for line in open(file):
            match = re.search(r'# !param (.*)', line);
            if match:
                s = match.group(1)
                s = re.sub(r'(\w+):', r'"\1":', s)
                s = re.sub(r':(\w+)', r'"\1"', s)
                s = "{" + s + "}"
                data = json.loads(s)

                key = data['key']
                del data['key']
                param[key] = data

        params[name] = param
    return params


def load_common_params(cd='./'):
    return params.common.get_initial()


class Common:
    def __init__(self):
        self.data = {
            'color_h': 0,
            'color_s': 0,
            'color_v': 1,
            'freq': 0.06,
            'freq_limit': 0,
            'beat': 1.0,
        }
        # self.color = [0, 0, 1] # hsv color
        # self.freq = 0.2

    def get_initial(self):
        data = {}
        data['color_h'] = {'value': self.data['color_h'], 'max': 360, 'min': 0 }
        data['color_s'] = {'value': self.data['color_s'], 'max': 1, 'min': 0 }
        data['color_v'] = {'value': self.data['color_v'], 'max': 1, 'min': 0 }
        data['freq'] = {'value': self.data['freq'], 'max': 2, 'min': 0 }
        data['freq_limit'] = {'value': self.data['freq_limit'], 'max': 20, 'min': 0 }
        data['beat'] = {'value': self.data['beat'], 'max': 2, 'min': 0 }
        return {'common_param': data}

    def set(self, data):
        self.data[data['name']] = data['val']
        # self.color[0] = params['color_h'] 
        # self.color[1] = params['color_s'] 
        # self.color[2] = params['color_v'] 
        # self.freq = params['freq'] 

    def get_bgr(self):
        keys = ['color_h', 'color_s', 'color_v', ]
        color = [self.data[key] for key in keys]
        color = cv2.cvtColor(np.array(color).astype(np.float32)[None, None, :], cv2.COLOR_HSV2BGR)
        color = color.reshape(3)
        return color

    def get_freq(self):
        return self.data['freq']

    def get_freq_limit(self):
        return self.data['freq_limit']

    def get_beat(self):
        return self.data['beat']

params = Params()
