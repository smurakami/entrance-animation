import cv2
import numpy as np

def alpha_brend(bottom, top, alpha):
    if len(alpha.shape) == 2 and len(bottom.shape) == 3:
        alpha = alpha[:, :, None]
    return bottom * (1 - alpha) + top * alpha

def add_brend(bottom, top, alpha):
    if len(alpha.shape) == 2 and len(bottom.shape) == 3:
        alpha = alpha[:, :, None]
    pixel = bottom + top * alpha
    pixel[pixel > 1] = 1
    pixel[pixel < 0] = 0
    return pixel

# class Mouse():
#     def __init__(self):
#         cv2.namedWindow('RAIN')
#         self.pos = np.zeros(2)
#         self.final_shape = None # set later
#         def callback(event, x, y, flags, param):
#             self.setMousePos(x, y)

#         cv2.setMouseCallback('RAIN', callback)

#     def setMousePos(self, x, y):
#         if self.final_shape == None:
#             return np.zeros(2)
#         h, w, _ = self.final_shape
#         self.pos = np.array([y / h, x / w])

#     @property
#     def x(self):
#         return self.pos[1]
    
#     @property
#     def y(self):
#         return self.pos[0]

# mouse = Mouse()


