import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *

import time

import osc.sound
from trigger import trigger

def __smoothstep_zero(x, smooth):
    x = np.array(x)
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5

    if len(x.shape) == 0:
        if x < -smooth:
            return 0
        elif x > smooth:
            return 1
        else:
            return y
    else:
        y[x < -smooth] = 0
        y[x > smooth] = 1
        return y

def smoothstep(x, th, smooth):
    if smooth == 0:
        return step(x, th)
    else:
        return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return float(x > th)


class Rainy2AOshimeri():
    # アスファルトにポタポタ落ちる雨
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for i in range(trigger.check()):
            if len(self.units) == 0:
                self.pulse()

        self.units = [drop for drop in self.units if not drop.to_remove]

        for drop in self.units:
            drop.update()

    def pulse(self):
        x = -2

        counter = 0
        while x < WIDTH:
            width_ = np.random.randint(1, 3)
            width = width_
            height = np.random.randint(1, 5)
            y = np.random.randint(HEIGHT)
            pos = np.array([y, x])
            width = min(WIDTH - x, width)

            unit = self.Unit(pos, width)
            unit.delay = counter * 0.02
            self.units.append(unit)

            x += width
            x += np.random.randint(0, 2)
            counter += 1

    def draw(self):
        frame = np.ones(self.shape) * 0.2
        # layers = []
        for drop in self.units:
            layer = drop.draw()
            alpha = layer[:, :, 3]
            bgr = layer[:, :, :3]
            frame[:, :, :3] = alpha_brend(frame[:, :, :3], bgr, alpha)
            frame[:, :, 3] += alpha

        frame[frame > 1] = 1
        return frame


    class Unit():
        # ポタポタ雨粒のシミュレーション。中で使う。
        def __init__(self, pos, width):
            self.shape = (HEIGHT, WIDTH, 4)
            # self.pos = np.array([np.random.randint(HEIGHT), np.random.randint(WIDTH)])
            self.pos = pos
            self.value = 1
            self.to_remove = False
            self.delay = 0.0

            y, x = self.pos
            # width = np.random.randint(1, 4)
            height = np.random.randint(1, 5)

            # width = min(WIDTH - x, width)
            height = min(HEIGHT - y, height)

            self.size = np.array([height, width])

            self.start_time = time.time()

        def update(self):
            age = time.time() - self.start_time
            self.value = smoothstep(age, 0.1 + self.delay, 0.1) * (1 - smoothstep(age, 1 + self.delay, 0.3))

            if self.value < 0.001 and age > self.delay + 0.1:
                self.to_remove = True

        def draw(self):
            frame = np.zeros(self.shape)
            color = params.common.get_bgr().tolist()
            y, x = self.pos
            height, width = self.size
            frame[y:y+height, x:x+width] =  color + [self.value]

            return frame # only

