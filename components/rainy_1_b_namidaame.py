import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y

def smoothstep(x, th, smooth):
    if smooth == 0:
        return step(x, th)
    else:
        return smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def line(shape, pos, length, smooth=0):
    ii, jj = np.indices(shape)
    y, x = pos
    pixel = (1 - step(jj, x)) * smoothstep(jj, x - length, smooth)
    pixel[ii != y] = 0
    return pixel


class Rainy1BNamidaame:
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        # color = np.ones((HEIGHT, WIDTH, 3))
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        # !param key: :max_alpha, min: 0, max: 1, value: 1.0
        max_alpha = params.fget(__file__, 'max_alpha') 

        alpha = alpha * max_alpha

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = np.array([np.random.randint(HEIGHT), 0]).astype(float)
            self.active = True

        def update(self):
            # !param key: :speed, min: 0, max: 10, value: 0.5
            speed = params.fget(__file__, 'speed')
            self.pos[1] += speed

        def draw(self):
            pixel = np.zeros(self.shape)
            # !param key: :smooth, min: 0, max: 10, value: 4.277
            smooth = params.fget(__file__, 'smooth')
            # !param key: :length, min: 0, max: 10, value: 1.878
            length = params.fget(__file__, 'length')
            pixel += line(pixel.shape, self.pos, length, smooth)
            if (pixel == 0).all():
                self.active = False
            return pixel

