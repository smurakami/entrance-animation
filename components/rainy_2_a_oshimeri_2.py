import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class Rainy2AOshimeri2:
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        # color = np.ones((HEIGHT, WIDTH, 3))
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        # !param key: :max_alpha, min: 0, max: 1, value: 1.0
        max_alpha = params.fget(__file__, 'max_alpha') 

        alpha = alpha * max_alpha

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = np.array([0, -3]).astype(float)
            self.active = True
            self.counter = 0

            if np.random.randint(2) == 0:
                self.pattern = np.array([
                    [0, 1, 0],
                    [1, 0, 1],
                    [0, 1, 0],
                    [0, 0, 0],
                ])
            else:
                self.pattern = np.array([
                    [0, 0, 0],
                    [1, 0, 1],
                    [0, 1, 0],
                    [1, 0, 1],
                ])

        def update(self):
            # !param key: :speed, min: 0, max: 10, value: 1.0
            speed = params.fget(__file__, 'speed')
            self.pos[1] += speed
            self.counter += 1

        def draw(self):
            pixel = np.zeros(self.shape)
            y, x_ = self.pos.astype(int)
            x = max(0, x_)
            width = min(WIDTH - x, 3)

            padding = x - x_

            pixel[:, x:x_+width] = self.pattern[:, padding:width]

            # pixel += line(pixel.shape, self.pos, length, smooth)
            if (pixel == 0).all() and self.counter > 20:
                self.active = False
            return pixel

