import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger
import colorsys


def get_random_color():
    # !param key: :hue_base, min: 0, max: 360, value: 360
    # !param key: :satulation_base, min: 0, max: 1, value: 1.0
    # !param key: :brightness_base, min: 0, max: 1, value: 1.0

    # !param key: :hue_sigma, min: 0, max: 360, value: 360
    # !param key: :satulation_sigma, min: 0, max: 1, value: 0
    # !param key: :brightness_sigma, min: 0, max: 1, value: 0

    hue_base = params.fget(__file__, 'hue_base')
    hue_sigma = params.fget(__file__, 'hue_sigma')

    satulation_base = params.fget(__file__, 'satulation_base')
    satulation_sigma = params.fget(__file__, 'satulation_sigma')

    brightness_base = params.fget(__file__, 'brightness_base')
    brightness_sigma = params.fget(__file__, 'brightness_sigma')

    h, s, v = np.random.random(3)

    hue = hue_base + h * hue_sigma
    satulation = satulation_base + s * satulation_sigma
    brightness = brightness_base + v * brightness_sigma

    hue = hue % 360
    satulation = max(0, satulation)
    satulation = min(1, satulation)
    brightness = max(0, brightness)
    brightness = min(1, brightness)

    rgb = colorsys.hsv_to_rgb(hue/360, satulation, brightness)
    return rgb[::-1]


def smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y

def smoothstep(x, th, smooth):
    if smooth == 0:
        return step(x, th)
    else:
        return smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

# def line(shape, pos, length):
#     ii, jj = np.indices(shape)
#     y, x = pos
#     pixel = (1 - step(ii, y)) * step(ii, y - length)
#     pixel[jj != x] = 0
#     return pixel


def circle(shape, pos, r, smooth=0):
    ii, jj = np.indices(shape)
    y, x = pos
    pixel = 1 - smoothstep(np.sqrt((ii - y) ** 2 + (jj - x) ** 2), r, smooth)
    return pixel


class Rainy3BTeppouame:
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        color = np.zeros((HEIGHT, WIDTH, 3))
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            a = unit.draw()
            c = np.ones((HEIGHT, WIDTH, 3)) * unit.color
            color = alpha_brend(color, c, a)
            alpha += a

        alpha[alpha > 1] = 1

        # !param key: :max_alpha, min: 0, max: 1, value: 1.0
        max_alpha = params.fget(__file__, 'max_alpha') 

        alpha = alpha * max_alpha

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.active = True
            # !param key: :radius, min: 0, max: 100, value: 21
            self.radius = params.fget(__file__, 'radius')
            self.pos = np.array([HEIGHT + self.radius, np.random.randint(WIDTH)]).astype(float)

            self.color = np.array(get_random_color())[None, None, :]

            self.counter = 0

        def update(self):
            # !param key: :speed, min: 0, max: 3, value: 1
            speed = params.fget(__file__, 'speed')
            self.pos[0] -= speed
            self.counter += 1

        def draw(self):
            pixel = np.zeros(self.shape)
            # !param key: :smooth, min: 0, max: 10, value: 0.5
            smooth = params.fget(__file__, 'smooth')
            pixel += circle(pixel.shape, self.pos, self.radius, smooth)

            if (pixel == 0).all() and self.counter > 20:
                self.active = False
            return pixel

