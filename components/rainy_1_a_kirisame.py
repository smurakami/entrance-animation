import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *

import osc.sound
from trigger import trigger

class Rainy1AKirisame():
    # アスファルトにポタポタ落ちる雨
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for i in range(trigger.check()):
            self.pulse()

        self.units = [drop for drop in self.units if not drop.to_remove]

        for drop in self.units:
            drop.update()

    def pulse(self):
        unit = self.Unit()
        self.units.append(unit)

    def draw(self):
        frame = np.ones(self.shape) * 0.2
        # layers = []
        for drop in self.units:
            layer = drop.draw()
            alpha = layer[:, :, 3]
            bgr = layer[:, :, :3]
            frame[:, :, :3] = alpha_brend(frame[:, :, :3], bgr, alpha)
            frame[:, :, 3] += alpha

        frame[frame > 1] = 1
        return frame


    class Unit():
        # ポタポタ雨粒のシミュレーション。中で使う。
        def __init__(self,):
            self.shape = (HEIGHT, WIDTH, 4)
            self.pos = np.array([np.random.randint(HEIGHT), np.random.randint(WIDTH)])
            self.value = 1
            self.to_remove = False

        def update(self):
            self.value -= 0.01
            if self.value < 0:
                self.to_remove = True

        def draw(self):
            frame = np.zeros(self.shape)
            color = params.common.get_bgr().tolist()
            frame[tuple(self.pos)] =  color + [self.value]

            return frame # only

        def drawSide(self, frame):
            # 大きい雨粒
            dirs = [
                [1, 0], [0, 1], [-1, 0], [0, -1],
            ]
            dirs = np.array(dirs)
            def valid(pos):
                return np.all([
                    pos[0] >= 0,
                    pos[0] < HEIGHT,
                    pos[1] >= 0,
                    pos[1] < WIDTH
                    ])
            for dir in dirs:
                pos = self.pos + dir
                if valid(pos):
                    frame[tuple(pos)] = [0, 0, 0, self.value]
