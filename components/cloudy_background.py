import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

import mathutils as u


class CloudyBackground():
    # 水面の波紋
    def __init__(self):
        shape = (HEIGHT, WIDTH)
        self.pos = np.zeros(shape)
        self.v = np.zeros(shape)

    def update(self):
        if trigger.flush() > 0:
            self.pulse()

        # !param key: :k, min: 0, max: 1, value: 0.1
        k = params.fget(__file__, 'k') # ばね係数
        # !param key: :kh, min: 0, max: 1, value: 0.2
        kh = params.fget(__file__, 'kh') # 水面伝搬係数
        # !param key: :f, min: 0, max: 1, value: 0.1
        f = params.fget(__file__, 'f') # 摩擦係数
        a = 0
        a += k * -self.pos # 垂直伝搬

        a += kh * self.propagation(self.pos)

        a += -f * self.v # 減衰
        self.v += a
        self.pos += self.v

    def propagation(self, map):
        # kh: 水平伝搬係数
        f = np.zeros_like(map)
        right = map[:, 1:] - map[:, :-1]
        f[:, :-1] += right
        left  = map[:, :-1] - map[:, 1:]
        f[:, 1:]  += left
        down  = map[1:, :] - map[:-1, :]
        f[:-1, :] += down
        up    = map[:-1, :] - map[1:, :]
        f[1:, :]  += up
        return f

    def pulse(self):
        # i = np.random.randint(HEIGHT)
        # j = np.random.randint(WIDTH)
        # !param key: :pulse, min: 0, max: 1, value: 0.1
        pulse = params.fget(__file__, 'pulse')
        for i in [1, 2]:
            j = WIDTH // 2
            self.v[i, j] = pulse

    def draw(self):
        value = self.pos
        # !param key: :hue_base, min: 0, max: 360, value: 280.31
        # !param key: :satulation_base, min: 0, max: 1, value: 0.661
        # !param key: :brightness_base, min: 0, max: 1, value: 1.0

        # !param key: :hue_sigma, min: -1000, max: 1000, value: -477.51
        # !param key: :satulation_sigma, min: -2, max: 2, value: 0
        # !param key: :brightness_sigma, min: -2, max: 2, value: 0

        hue_base = params.fget(__file__, 'hue_base')
        hue_sigma = params.fget(__file__, 'hue_sigma')

        satulation_base = params.fget(__file__, 'satulation_base')
        satulation_sigma = params.fget(__file__, 'satulation_sigma')

        brightness_base = params.fget(__file__, 'brightness_base')
        brightness_sigma = params.fget(__file__, 'brightness_sigma')

        hue = hue_base + value * hue_sigma
        satulation = satulation_base + value * satulation_sigma
        brightness = brightness_base + value * brightness_sigma

        hue = hue % 360
        # hue[hue < 0] = 0
        # hue[hue > ] = 1
        satulation[satulation < 0] = 0
        satulation[satulation > 1] = 1
        brightness[brightness < 0] = 0
        brightness[brightness > 1] = 1

        frame = np.dstack([hue, satulation, brightness]).astype(np.float32)

        frame = cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)
        # frame = np.dstack([frame] * 3)
        return frame


