import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

import time

def __smoothstep_zero(x, smooth):
    x = np.array(x)
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5

    if len(x.shape) == 0:
        if x < -smooth:
            return 0
        elif x > smooth:
            return 1
        else:
            return y
    else:
        y[x < -smooth] = 0
        y[x > smooth] = 1
        return y

def smoothstep(x, th, smooth):
    return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def upline(shape, pos, smooth=2):
    if smooth > 0:
        s = lambda x: smoothstep(x, 0, smooth)
    else:
        s = step
    width = 4
    length = 15
    w = width + smooth/2
    ii, jj = np.indices(shape)
    y, x = pos
    up = s(jj - x + w * 0.5 / np.sqrt(2) + y - ii) - s(jj - x - w * 0.5 / np.sqrt(2) + y - ii)
    down = s(-(jj - x) + length * 0.5 / np.sqrt(2) + y - ii) - s(-(jj - x) - length * 0.5 / np.sqrt(2) + y - ii)
    return up * down


class EndRainning:
    # 振り始めの演出
    def __init__(self):
        self.units = [
            Phase1,
            Phase2,
            Phase3,
            # Phase4,
            # Phase5,
            # Phase6,
        ]
        self.unit_index = 0
        self.unit = self.units[self.unit_index]()
        self.is_active = True

    def update(self):
        self.unit.update()
        if not self.unit.is_active:
            self.unit_index += 1

            if self.unit_index < len(self.units):
                self.unit = self.units[self.unit_index]()
            else:
                self.is_active = False

    def draw(self):
        return self.unit.draw()


class Phase1:
    # Special演出
    # 白くなっていく
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.finished = False
        self.counter = 0
        self.is_active = True
        # !param key: :speed_phase1, min: 0, max: 1, value: 0.01
        self.speed = params.fget(__file__, 'speed_phase1')

    def update(self):
        self.counter += 1
        self.speed = params.fget(__file__, 'speed_phase1')

        if self.speed * self.counter > 1:
            if time.time() - self.next_timer > 2.0:
                self.is_active = False
        else:
            self.next_timer = time.time()

    def draw(self):
        pixel = np.ones(self.shape)
        value = self.counter * self.speed
        value = min(1, value)
        pixel[:, :, 3] = value
        return pixel


class Phase2:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.finished = False
        self.counter = 0
        self.is_active = True
        self.next_timer = time.time()

        def makehsv(h, s, v):
            hsv = [h, s, v]
            hsv = np.array(hsv)[None, None, :].astype(np.float32)
            color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)
            return np.array(color)

        num = 7
        self.units = []
        for i in range(-3, num + 3):
            pos = np.array([HEIGHT/2 - 0.5, WIDTH / num * i])
            unit = self.Unit(pos)
            unit.color = makehsv(360/num * i, 1, 1)
            self.units.append(unit)


    def update(self):
        self.counter += 1

        if self.counter % 2 == 0:
            cand = [unit for unit in self.units if unit.started == False]
            if len(cand) > 0:
                unit = np.random.choice(cand)
                unit.pulse()

        if np.all([unit.started for unit in self.units]):
            if time.time() - self.next_timer > 1:
                self.is_active = False
        else:
            self.next_timer = time.time()

        for unit in self.units:
            unit.update()


    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3))

        for unit in self.units:
            pixel = unit.draw()
            alpha = pixel[:, :, 3]
            pixel = pixel[:, :, :3]
            color = alpha_brend(color, pixel, alpha)

        alpha = np.ones((HEIGHT, WIDTH))
        return np.dstack([color, alpha])

    class Unit:
        def __init__(self, pos):
            self.pos_end = pos
            self.color = None
            self.showing = False
            # !param key: :speed_phase2, min: 0, max: 2, value: 0.1
            self.speed = params.fget(__file__, 'speed_phase2')
            self.dir = np.array([1, 1])
            if np.random.random() > 0.5:
                self.dir *= -1
            self.pos = self.pos_end - self.dir * 6
            self.pos_begin = self.pos
            self.counter = 0
            self.started_counter = 0
            self.started = False

        def update(self):

            val = smoothstep(self.speed * self.started_counter, 1, 1)
            self.pos = self.pos_end * val + self.pos_begin * (1 - val)
            if self.started:
                self.started_counter += 1
            self.counter += 1

        def pulse(self):
            self.started = True

        def draw(self):
            color = np.ones((HEIGHT, WIDTH, 3)) * self.color
            alpha = np.zeros((HEIGHT, WIDTH))

            ii, jj = np.indices((HEIGHT, WIDTH))

            alpha += upline(alpha.shape, self.pos)
            return np.dstack([color, alpha])


class Phase3:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.finished = False
        self.counter = 0
        self.is_active = True
        self.next_timer = time.time()
        self.timer = time.time()
        self.val = 1

        def makehsv(h, s, v):
            hsv = [h, s, v]
            hsv = np.array(hsv)[None, None, :].astype(np.float32)
            color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)
            return np.array(color)

        num = 7
        self.units = []
        for i in range(-5, num + 5):
            unit = self.Unit()
            unit.color = makehsv(360/num * i, 1, 1)
            pos = np.array([HEIGHT/2 - 0.5, WIDTH / num * i]).astype(np.float32)
            unit.pos = pos
            self.units.append(unit)


    def update(self):
        self.counter += 1

        age = time.time() - self.timer

        self.val = 1 - smoothstep(age, 5, 2)
        if self.val == 0:
            self.is_active = False

        for unit in self.units:
            unit.update()


    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3))

        for unit in self.units:
            pixel = unit.draw()
            alpha = pixel[:, :, 3]
            pixel = pixel[:, :, :3]
            color = alpha_brend(color, pixel, alpha)

        alpha = np.ones((HEIGHT, WIDTH))
        alpha *= self.val
        return np.dstack([color, alpha])


    class Unit:
        def __init__(self):
            self.pos = None
            self.color = None
            self.showing = False
            self.speed = np.array([0, 0]).astype(np.float32)
            self.dir = np.array([1, 1])
            self.val = 1

        def update(self):
            self.speed += np.array([0, 0.02])
            self.pos += self.speed
            
            num = 7
            if self.pos[1] > WIDTH/num * (num + 4):
                self.pos[1] = WIDTH/num * -5

        def pulse(self):
            pass

        def draw(self):
            color = np.ones((HEIGHT, WIDTH, 3)) * self.color
            alpha = np.zeros((HEIGHT, WIDTH))

            ii, jj = np.indices((HEIGHT, WIDTH))

            # alpha[(self.x - 1 < jj) & (self.x + 1 > jj)] = 1
            alpha += upline(alpha.shape, self.pos)
            return np.dstack([color, alpha])


