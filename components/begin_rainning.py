import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

import time

class BeginRainning:
    # 振り始めの演出
    def __init__(self):
        self.units = [
            Phase1,
            Phase2,
            Phase3,
            Phase4,
            Phase5,
            Phase6,
        ]
        self.unit_index = 0
        self.unit = self.units[self.unit_index]()
        self.is_active = True

    def update(self):
        self.unit.update()
        if not self.unit.is_active:
            self.unit_index += 1
            if self.unit_index < len(self.units):
                self.unit = self.units[self.unit_index]()
            else:
                self.is_active = False

    def draw(self):
        return self.unit.draw()


class Phase1:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.finished = False
        self.counter = 0
        self.is_active = True
        self.next_timer = time.time()

    def update(self):
        self.counter += 1
        self.speed = WIDTH / 10

        if self.speed * self.counter > WIDTH:
            if time.time() - self.next_timer > 2/3:
                self.is_active = False
        else:
            self.next_timer = time.time()

    def draw(self):
        # !param key: :color_phase1, is_color: true, value: [62.401, 1, 1]
        hsv = params.fget(__file__, 'color_phase1')
        hsv = np.array(hsv)[None, None, :]
        hsv = np.ones((HEIGHT, WIDTH, 3), dtype=np.float32) * hsv
        color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)

        ii, jj = np.indices((HEIGHT, WIDTH))
        alpha = jj <= self.counter * self.speed

        alpha[ii % 2 == 0] = alpha[ii % 2 == 0][::-1]

        alpha = alpha.astype(float)
        pixel = np.dstack([color, alpha])
        return pixel


class Phase2:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.finished = False
        self.counter = 0
        self.is_active = True
        self.next_timer = time.time()
        self.speed = 1

    def update(self):
        self.counter += 1
        # if (WIDTH + 1) // 2 - self.counter * self.speed > 0:
        #     self.finished = True

        self.finished = (WIDTH - 0 + 1) // 2 - self.counter * self.speed <=  0

        if self.finished:
            if time.time() - self.next_timer > 0.3:
                self.is_active = False
        else:
            self.next_timer = time.time()


    def draw(self):
        hsv = params.fget(__file__, 'color_phase1')
        hsv = np.array(hsv)[None, None, :]
        hsv = np.ones((HEIGHT, WIDTH, 3), dtype=np.float32) * hsv
        base_color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)

        # !param key: :color_phase2_1_top, is_color: true, value: [35.59, 1, 1]
        hsv = params.fget(__file__, 'color_phase2_1_top')
        hsv = np.array(hsv)[None, None, :].astype(np.float32)
        top_color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)

        ii, jj = np.indices((HEIGHT, WIDTH))

        color = base_color.copy()
        color[((jj + 1) // 2 + ii // 2) % 2 == 0] = top_color

        color[(WIDTH - jj + 1) // 2 - self.counter * self.speed > 0] = base_color[0:1, 0:1]

        # alpha = jj <= self.counter * self.speed
        # alpha[ii % 2 == 0] = alpha[ii % 2 == 0][::-1]

        alpha = np.ones((HEIGHT, WIDTH))
        pixel = np.dstack([color, alpha])
        return pixel


class Phase3:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.finished = False
        self.speed = params.fget(__file__, 'speed_phase3')
        self.counter = int(1.0 / self.speed) - 10
        self.is_active = True
        self.speed = 0

    def update(self):
        self.counter += 1
        # !param key: :speed_phase3, min: 0, max: 1, value: 0.04
        self.speed = params.fget(__file__, 'speed_phase3')

        # if (WIDTH + 1) // 2 - self.counter * self.speed > 0:
        #     self.finished = True

        self.finished = self.counter * self.speed > 2.7

        if self.finished:
            if time.time() - self.next_timer > 0:
                self.is_active = False
        else:
            self.next_timer = time.time()


    def draw(self):
        def makeColor(name):
            hsv = params.fget(__file__, name)
            hsv = np.array(hsv)[None, None, :].astype(np.float32)
            color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)
            return color

        # !param key: :color_phase3_2_base, is_color: true, value: [93.221, 0.547, 1]
        # !param key: :color_phase3_2_top, is_color: true, value: [136.572, 1, 0.787]
        # !param key: :color_phase3_3_base, is_color: true, value: [304.35, 0.588, 1]
        # !param key: :color_phase3_3_top, is_color: true, value: [360, 1, 1]
        base_colors = [
            makeColor('color_phase1'),
            makeColor('color_phase3_2_base'),
            makeColor('color_phase3_3_base'),
        ]

        top_colors = [
            makeColor('color_phase2_1_top'),
            makeColor('color_phase3_2_top'),
            makeColor('color_phase3_3_top'),
        ]

        index = min(int(self.counter * self.speed), len(base_colors) - 1)
        base_color = base_colors[index]
        top_color = top_colors[index]

        ii, jj = np.indices((HEIGHT, WIDTH))

        color = np.zeros((HEIGHT, WIDTH, 3))
        color[:] = base_color
        color[((jj + 1) // 2 + ii // 2) % 2 == 0] = top_color

        # alpha = jj <= self.counter * self.speed
        # alpha[ii % 2 == 0] = alpha[ii % 2 == 0][::-1]

        alpha = np.ones((HEIGHT, WIDTH))
        pixel = np.dstack([color, alpha])
        return pixel


class Phase4:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.finished = False
        self.counter = 0
        self.is_active = True
        self.speed = 0.2

    def update(self):
        self.counter += 1
        # !param key: :speed_phase4, min: 0, max: 1, value: 0.07
        self.speed = params.fget(__file__, 'speed_phase4')

        # if (WIDTH + 1) // 2 - self.counter * self.speed > 0:
        #     self.finished = True

        # self.finished = self.counter * self.speed > 3
        self.finished = self.counter * self.speed > 6

        if self.finished:
            if time.time() - self.next_timer > 0.3:
                self.is_active = False
        else:
            self.next_timer = time.time()


    def draw(self):
        def makeColor(name):
            hsv = params.fget(__file__, name)
            hsv = np.array(hsv)[None, None, :].astype(np.float32)
            color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)
            return color

        base_color = makeColor('color_phase3_3_base')
        top_color = makeColor('color_phase3_3_top')

        ii, jj = np.indices((HEIGHT, WIDTH))

        color = np.zeros((HEIGHT, WIDTH, 3))
        color[:] = base_color
        color[((jj + 1) // 2 + ii // 2) % 2 == 0] = top_color

        # !param key: :color_phase4, is_color: true, value: [182.943, 0.509, 1]
        if self.counter * self.speed > 5:
            color[:] = makeColor('color_phase4')

        if not self.finished:
            phase = (-np.cos(self.counter * self.speed * np.pi) + 1) / 2
            phase = phase ** 6

            color[1 - abs(1 - (jj + 0.5) / (WIDTH/2)) - phase * 1.2 < 0] = 1

        alpha = np.ones((HEIGHT, WIDTH))
        pixel = np.dstack([color, alpha])
        return pixel


class Phase5:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.is_active = True
        # self.pixel = np.ones(self.shape)
        self.units = []
        self.next_flg = False
        self.color_index = 0
        self.counter = 0

    def update(self):
        self.counter += 1

        # self.units.append(self.Unit())

        if self.counter % 3 == 0:
            xs = np.arange(WIDTH)
            cand = np.ones(WIDTH, dtype=bool)

            for unit_ in self.units:
                cand[unit_.x] = False

            if cand.sum() > 0:
                num = min(len(xs[cand]), 4)
                xs_ = np.random.choice(xs[cand], num, False)
                for x in xs_:
                    unit = self.Unit()
                    unit.x = x
                    self.units.append(unit)

        if self.next_flg:
            if time.time() - self.next_timer > 0.5:
                self.color_index += 1
                self.units = []
                self.next_flg = False

                if self.color_index >= 3:
                    self.is_active = False
        else:
            self.next_timer = time.time()

        for unit in self.units:
            unit.update()

    def draw(self):
        def makeColor(name):
            hsv = params.fget(__file__, name)
            hsv = np.array(hsv)[None, None, :].astype(np.float32)
            color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)
            return color

        # !param key: :color_phase5_1, is_color: true, value: [223.342, 1, 1]
        # !param key: :color_phase5_2, is_color: true, value: [65.93, 1, 1]
        # !param key: :color_phase5_3, is_color: true, value: [32.522, 1, 1]
        base_colors = [
            makeColor('color_phase4'),
            makeColor('color_phase5_1'),
            makeColor('color_phase5_2'),
        ]

        top_colors = [
            makeColor('color_phase5_1'),
            makeColor('color_phase5_2'),
            makeColor('color_phase5_3'),
        ]

        color = np.ones((HEIGHT, WIDTH, 3))
        color[:] = base_colors[self.color_index]

        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        if (alpha == 1).all():
            self.next_flg = True

        top = np.ones((HEIGHT, WIDTH, 3)) * np.array( top_colors[self.color_index] )
        color = alpha_brend(color, top, alpha)

        alpha = np.ones((HEIGHT, WIDTH))
        return np.dstack([color, alpha])

    class Unit:
        def __init__(self):
            self.x = int(np.random.random() * WIDTH)

        def update(self):
            pass

        def draw(self):
            alpha = np.zeros((HEIGHT, WIDTH))
            alpha[:, int(self.x)] = 1

            return alpha


class Phase6:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.is_active = True
        self.counter = 0

    def update(self):
        self.counter += 1

        # if self.next_flg:
        #     if time.time() - self.next_timer > 0.5:
        #         self.color_index += 1
        #         self.units = []
        #         self.next_flg = False

        #         if self.color_index >= 3:
        #             self.is_active = False
        # else:
        #     self.next_timer = time.time()

        # for unit in self.units:
        #     unit.update()

    def draw(self):
        def makeColor(name):
            hsv = params.fget(__file__, name)
            hsv = np.array(hsv)[None, None, :].astype(np.float32)
            color = cv2.cvtColor(hsv.astype(np.float32), cv2.COLOR_HSV2BGR)
            return color

        color = np.ones((HEIGHT, WIDTH, 3)) * np.array(makeColor('color_phase5_3'))
        alpha = np.ones((HEIGHT, WIDTH))

        pos = WIDTH - self.counter * 1
        ii, jj = np.indices((HEIGHT, WIDTH))

        if pos < 0:
            self.is_active = False

        alpha[jj >= pos] = 0
        color[(pos - 2 < jj) & (jj < pos)] = 1

        return np.dstack([color, alpha])

