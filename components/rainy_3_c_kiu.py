import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

import colorsys


def get_random_color():
    # !param key: :hue_base, min: 0, max: 360, value: 360
    # !param key: :satulation_base, min: 0, max: 1, value: 1.0
    # !param key: :brightness_base, min: 0, max: 1, value: 1.0

    # !param key: :hue_sigma, min: 0, max: 360, value: 360
    # !param key: :satulation_sigma, min: 0, max: 1, value: 0
    # !param key: :brightness_sigma, min: 0, max: 1, value: 0

    hue_base = params.fget(__file__, 'hue_base')
    hue_sigma = params.fget(__file__, 'hue_sigma')

    satulation_base = params.fget(__file__, 'satulation_base')
    satulation_sigma = params.fget(__file__, 'satulation_sigma')

    brightness_base = params.fget(__file__, 'brightness_base')
    brightness_sigma = params.fget(__file__, 'brightness_sigma')

    h, s, v = np.random.random(3)

    hue = hue_base + h * hue_sigma
    satulation = satulation_base + s * satulation_sigma
    brightness = brightness_base + v * brightness_sigma

    hue = hue % 360
    satulation = max(0, satulation)
    satulation = min(1, satulation)
    brightness = max(0, brightness)
    brightness = min(1, brightness)

    rgb = colorsys.hsv_to_rgb(hue/360, satulation, brightness)
    return np.array(rgb[::-1])[None, None, :]


def __smoothstep_zero(x, smooth):
    x = np.array(x)
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5

    if len(x.shape) == 0:
        if x < -smooth:
            return 0
        elif x > smooth:
            return 1
        else:
            return y
    else:
        y[x < -smooth] = 0
        y[x > smooth] = 1
        return y

def smoothstep(x, th, smooth):
    if smooth == 0:
        return step(x, th)
    else:
        return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def line_(shape, pos, direction, smooth=0):
    ii, jj = np.indices(shape)
    y, x = pos
    ii = ii - y
    jj = jj - x
    b, a = direction

    line = smoothstep(b * ii + a * jj, 0, smooth)
    return line

def line(shape, pos, direction, width, smooth=0):
    norm = np.array([direction[0], direction[1]])
    up = line_(shape, pos - norm * (width + smooth) / 2, direction, smooth)
    down = 1 - line_(shape, pos + norm * (width + smooth) / 2, direction, smooth)
    return up * down


def rotate(x, a):
    s = np.sin(a)
    c = np.cos(a)
    mat = np.array([[c, -s], [s, c] ])
    return np.matmul(x, mat)


def to_binary(x):
    return x * 2 - 1

def corn(shape, pos, direction, angle, smooth=0):
    norm = np.array([direction[0], direction[1]])
    d = rotate(direction, angle/2)
    up = line_(shape, pos, d, smooth)
    d = rotate(direction, -angle/2)
    down = 1 - line_(shape, pos, d, smooth)
    value = to_binary(up) * to_binary(down)
    value[value < 0] = 0
    return value

class Rainy3CKiu:
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        color = np.zeros((HEIGHT, WIDTH, 3))
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            a = unit.draw()
            c = np.ones((HEIGHT, WIDTH, 3)) * unit.color
            color = alpha_brend(color, c, a)
            alpha += a

        alpha[alpha > 1] = 1

        # !param key: :max_alpha, min: 0, max: 1, value: 1.0
        max_alpha = params.fget(__file__, 'max_alpha') 

        alpha = alpha * max_alpha

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.active = True
            self.angle = 0
            self.base_angle = np.random.uniform(np.pi)
            self.pos = np.array([HEIGHT/2, WIDTH/2])
            self.color = get_random_color()
            self.counter = 0
            self.alpha = 0

        def update(self):
            # !param key: :speed, min: 0, max: 1, value: 0.1
            speed = params.fget(__file__, 'speed')
            self.angle += speed
            self.alpha = smoothstep(self.angle, 0.5, 0.5) * (1 - smoothstep(self.angle, np.pi * 5, 1))
            self.counter += 1

        def draw(self):
            pixel = np.zeros(self.shape)
            # !param key: :smooth, min: 0, max: 10, value: 1.0
            smooth = params.fget(__file__, 'smooth')
            # !param key: :width, min: 0, max: 10, value: 1.00
            width = params.fget(__file__, 'width')
            angle = self.angle + self.base_angle
            direction = np.array([np.sin(angle), np.cos(angle)])
            pixel += line(pixel.shape, self.pos, direction, np.pi/6, smooth)

            pixel *= self.alpha

            if (pixel == 0).all() and self.counter > 10 :
                self.active = False
            return pixel

