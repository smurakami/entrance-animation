import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

import time


def __smoothstep_zero(x, smooth):
    if x < -smooth:
        return 0
    elif x > smooth:
        return 1
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    return y

def smoothstep(x, th, smooth):
    if smooth == 0:
        return step(x, th)
    else:
        return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)


class MidLayer:
    # ガウス分布で虹を描く
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 3)
        self.counter = 0

        self.units = []
        for i in range(7):
            unit = self.Unit()
            unit.pos = np.array([0.5, 1/6 * (i)])
            unit.beat_phase = i % 2
            self.units.append(unit)

    def update(self):
        for unit in self.units:
            unit.update()

        # !param key: :smooth, min: 0, max: 1, value: 0.2
        self.smooth = params.fget(__file__, 'smooth')

        if trigger.beat.check() > 0:
            self.beat = time.time() + self.smooth


    def draw(self):
        value = np.zeros((HEIGHT, WIDTH))
        for unit in self.units:
            value += unit.draw()

        value[value > 1] = 1
        value[value < -1] = -1

        value = (value + 1) / 2

        hsv = np.zeros(self.shape, dtype=np.float32)
        # !param key: :hsv_h, min: 0, max: 360, value: 190
        hsv[:, :, 0] = value * 180 + params.fget(__file__, 'hsv_h')
        # !param key: :hsv_s, min: 0, max: 1, value: 1
        hsv[:, :, 1] = params.fget(__file__, 'hsv_s')
        # !param key: :hsv_v, min: 0, max: 1, value: 1
        hsv[:, :, 2] = params.fget(__file__, 'hsv_v')

        pixel = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        return pixel

    class Unit:
        # ガウス分布の中心点
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = np.random.random(2)
            self.sign = 1
            self.beat = time.time() - 100
            self.beat_phase = 0
            self.beat_count = 0

        def update(self):
            # !param key: :smooth, min: 0, max: 1, value: 0.2
            self.smooth = params.fget(__file__, 'smooth')
            if trigger.beat.check() > 0:
                self.beat_count += 1
                if self.beat_count % 2 == self.beat_phase:
                    self.beat = time.time() + self.smooth / 2

        def get_beat(self):
            return smoothstep(time.time(), self.beat - self.smooth / 2, self.smooth) * (1 - smoothstep(time.time(), self.beat + self.smooth / 2, self.smooth))

        def draw(self):
            pixel = np.zeros(self.shape)
            ii, jj = np.indices(self.shape)
            ys = (ii + 0.5) / HEIGHT
            xs = (jj + 0.5) / WIDTH
            dxs = xs - self.pos[1]
            # アスペクト比を補正する
            dys = (ys - self.pos[0]) * HEIGHT/WIDTH
            # !param key: :sigma, min: 0, max: 1, value: 0.074
            sigma = params.fget(__file__, 'sigma')
            val = (np.exp(-(dxs ** 2 + dys ** 2) / (2 * sigma ** 2))) / np.sqrt(2*np.pi)

            val *= self.get_beat()

            pixel[ii, jj] = val
            return pixel * self.sign
