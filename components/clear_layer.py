import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class ClearLayer:
    # 透明なレイヤー
    def __init__(self):
        self.pixel = np.zeros((HEIGHT, WIDTH, 4))

    def update(self):
        pass

    def draw(self):
        return self.pixel


