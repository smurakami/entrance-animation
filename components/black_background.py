import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class BlackBackground:
    # ガウス分布で虹を描く
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 3)

    def update(self):
        pass

    def draw(self):
        return np.zeros(self.shape)

