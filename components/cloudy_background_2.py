import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

import mathutils as u

from weather import weather


def wave(shape, pos, radius, smooth):
    ii, jj = np.indices(shape)
    dist = np.sqrt((ii - pos[0]) ** 2 + (jj - pos[1]) ** 2)
    pixel = u.smoothstep(dist, radius - smooth, smooth) - u.smoothstep(dist, radius + smooth, smooth)
    return pixel


class CloudyBackground2:
    # 水面の波紋
    def __init__(self):
        shape = (HEIGHT, WIDTH, 3)
        self.shape = shape
        self.pos = np.zeros(shape)
        self.v = np.zeros(shape)
        self.units = []

    def update(self):
        self.units = [unit for unit in self.units if unit.is_active]

        if trigger.flush() > 0:
            self.pulse()

        for unit in self.units:
            unit.update()

    def pulse(self):
        unit = self.Unit()
        self.units.append(unit)

    def draw(self):
        value = np.zeros((HEIGHT, WIDTH))
        for unit in self.units:
            value += unit.draw()
        print(len(self.units))

        value[value > 1] = 1
        value[value < 0] = 0

        pixel = np.ones(self.shape)
        pixel = pixel * value.reshape((HEIGHT, WIDTH, 1))

        color = np.ones(self.shape) * (np.array([195, 0, 130])[None, None, :] / 255)

        # color = weather.get_wind_color()
        # color = weather.get_wind_color()

        pixel = color + pixel
        pixel[pixel > 1] = 1

        return pixel

    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = np.array([HEIGHT, WIDTH]) / 2
            self.radius = 0
            self.speed = 0.3
            self.is_active = True

        def update(self):
            self.radius += self.speed
            if self.radius > WIDTH * 2:
                self.is_active = False

        def draw(self):
            return wave(self.shape, self.pos, self.radius, 0.5)

