import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

import colorsys


def get_random_color():
    # !param key: :hue_base, min: 0, max: 360, value: 360
    # !param key: :satulation_base, min: 0, max: 1, value: 1.0
    # !param key: :brightness_base, min: 0, max: 1, value: 1.0

    # !param key: :hue_sigma, min: 0, max: 360, value: 360
    # !param key: :satulation_sigma, min: 0, max: 1, value: 0
    # !param key: :brightness_sigma, min: 0, max: 1, value: 0

    hue_base = params.fget(__file__, 'hue_base')
    hue_sigma = params.fget(__file__, 'hue_sigma')

    satulation_base = params.fget(__file__, 'satulation_base')
    satulation_sigma = params.fget(__file__, 'satulation_sigma')

    brightness_base = params.fget(__file__, 'brightness_base')
    brightness_sigma = params.fget(__file__, 'brightness_sigma')

    h, s, v = np.random.random(3)

    hue = hue_base + h * hue_sigma
    satulation = satulation_base + s * satulation_sigma
    brightness = brightness_base + v * brightness_sigma

    hue = hue % 360
    satulation = max(0, satulation)
    satulation = min(1, satulation)
    brightness = max(0, brightness)
    brightness = min(1, brightness)

    rgb = colorsys.hsv_to_rgb(hue/360, satulation, brightness)
    return rgb[::-1]


def smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y

def smoothstep(x, th, smooth):
    if smooth == 0:
        return step(x, th)
    else:
        return smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def h_line(shape, pos, length, smooth=0):
    ii, jj = np.indices(shape)
    y, x = pos
    pixel = (1 - step(jj, x)) * smoothstep(jj, x - length, smooth)
    pixel[ii != y] = 0
    return pixel

def v_line(shape, pos, length, smooth=0):
    ii, jj = np.indices(shape)
    y, x = pos
    pixel = (1 - step(ii, y)) * smoothstep(ii, y - length, smooth)
    pixel[jj != x] = 0
    return pixel


class Rainy3AShinotsukuame:
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        color = np.zeros((HEIGHT, WIDTH, 3))
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            a = unit.draw()
            c = np.ones((HEIGHT, WIDTH, 3)) * unit.color
            color = alpha_brend(color, c, a)
            alpha += a

        alpha[alpha > 1] = 1

        # !param key: :max_alpha, min: 0, max: 1, value: 1.0
        max_alpha = params.fget(__file__, 'max_alpha') 

        alpha = alpha * max_alpha

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)

            if np.random.randint(2) == 0:
                self.horizontal = True
                self.pos = np.array([np.random.randint(HEIGHT), 0]).astype(float)
                self.direction = np.array([0, 1])
            else:
                self.horizontal = False
                self.pos = np.array([0, np.random.randint(WIDTH)]).astype(float)
                self.direction = np.array([1, 0])
            self.flipped = np.random.randint(2)
            self.active = True
            self.color = np.array(get_random_color())[None, None, :]

        def update(self):
            # !param key: :speed, min: 0, max: 10, value: 2
            speed = params.fget(__file__, 'speed')
            self.pos += speed * self.direction

        def draw(self):
            pixel = np.zeros(self.shape)
            # !param key: :smooth, min: 0, max: 10, value: 4.277
            smooth = params.fget(__file__, 'smooth')
            # !param key: :length, min: 0, max: 100, value: 100
            length = params.fget(__file__, 'length')
            if self.horizontal:
                pixel += h_line(pixel.shape, self.pos, length, smooth)
            else:
                pixel += v_line(pixel.shape, self.pos, length, smooth)

            if self.flipped:
                pixel = pixel[::-1, ::-1]

            if (pixel == 0).all():
                self.active = False
            return pixel

