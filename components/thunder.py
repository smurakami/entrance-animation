import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
import time

import osc.sound
from trigger import trigger

import mathutils as u

class Thunder():
    # 雷
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        # make pattern
        self.pattern = self.make_pattern()
        self.start_time = time.time()
        self.value = 1
        self.is_active = True

    def make_pattern(self):
        pixel = np.zeros((HEIGHT, WIDTH), dtype=float)
        pos = np.array([np.random.randint(HEIGHT), 0])
        y, x = pos

        while x < WIDTH:
            pixel[y, x] = 1

            y += np.random.randint(-1, 2)
            if y < 0:
                y = 0
            if y >= HEIGHT:
                y = HEIGHT-1
            x += 1

        return pixel

    def update(self):
        self.value = 1 - u.smoothstep(time.time() - self.start_time, 1, 0.5)
        if self.value == 0:
            self.is_active = False

    def draw(self):
        pixel = np.zeros(self.shape, dtype=float)

        pixel[:, :, :3] = np.array([0, 1, 1]).astype(float)
        pixel[:, :, 3] = self.pattern * self.value

        return pixel


