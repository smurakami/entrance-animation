import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

import time


def __smoothstep_zero(x, smooth):
    if x < -smooth:
        return 0
    elif x > smooth:
        return 1
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    return y

def smoothstep(x, th, smooth):
    if smooth == 0:
        return step(x, th)
    else:
        return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)


class Kitsune:
    # ガウス分布で虹を描く
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 3)
        self.counter = 0
        self.beat_counter = 0
        self.is_active = True
        self.start_time = time.time()
        self.alpha = 0

        self.units = []
        for i in range(4):
            unit = self.Unit()
            unit.pos = np.array([2 - i % 2, 2 + 5 * i])
            unit.beat_phase = i % 2
            unit.delay = i * 0.1
            self.units.append(unit)

    def update(self):
        for unit in self.units:
            unit.update()

        # !param key: :smooth, min: 0, max: 1, value: 0.2
        self.smooth = params.fget(__file__, 'smooth')

        if trigger.beat.check() > 0:
            self.beat_counter += 1
            if self.beat_counter % 2 == 0:
                self.beat = time.time() + self.smooth

        up = smoothstep(time.time() - self.start_time, 1, 1)
        down = 1 - smoothstep(time.time() - self.start_time, 10, 1)

        self.alpha = up * down

        if time.time() > self.start_time + 1 and self.alpha == 0:
            self.is_active = False

    def draw(self):
        value = np.zeros((HEIGHT, WIDTH))
        for unit in self.units:
            value += unit.draw()

        value[value > 1] = 1
        value[value < -1] = -1

        value = (value + 1) / 2

        hsv = np.zeros(self.shape, dtype=np.float32)
        # !param key: :hsv_h, min: 0, max: 360, value: 190
        hsv[:, :, 0] = value * 180 + params.fget(__file__, 'hsv_h')
        # !param key: :hsv_s, min: 0, max: 1, value: 1
        hsv[:, :, 1] = params.fget(__file__, 'hsv_s')
        # !param key: :hsv_v, min: 0, max: 1, value: 1
        hsv[:, :, 2] = params.fget(__file__, 'hsv_v')
        pixel = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

        alpha = value.copy()

        # blue = np.ones(self.shape) * np.array([1, 1, 0]).reshape([1, 1, 3])

        alpha = (alpha - 0.5) * 2
        # !param key: :power, min: 0, max: 5, value: 4
        power = params.fget(__file__, 'power')

        alpha *= power

        alpha[alpha > 1] = 1
        alpha[alpha < 0] = 0

        # pixel = alpha_brend(blue, pixel, alpha)

        alpha *= self.alpha

        return np.dstack([pixel, alpha])

    class Unit:
        # ガウス分布の中心点
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = np.random.random(2)
            self.sign = 1
            self.beat = time.time() - 100
            self.beat_phase = 0
            self.beat_count = 0
            self.smooth = params.fget(__file__, 'smooth')
            self.delay = 0

        def update(self):
            # !param key: :smooth, min: 0, max: 1, value: 0.9
            self.smooth = params.fget(__file__, 'smooth')
            if trigger.beat.check() > 0:
                self.beat_count += 1
                if self.beat_count % 2 == 0:
                    if self.beat_count / 2 % 2 == self.beat_phase:
                        self.beat = time.time() + self.smooth + self.delay


        def get_beat(self):
            up =  smoothstep(time.time(), self.beat - self.smooth / 2, self.smooth)
            down = (1 - smoothstep(time.time(), 1 + self.beat + self.smooth / 2, self.smooth))
            return up * down

        def draw(self):
            pixel = np.zeros((HEIGHT, WIDTH), dtype=float)
            y, x = self.pos

            for dx in [-1, 1]:
                pixel[y, x + dx] = 1
            for dy in [-1, 1]:
                pixel[y + dy, x] = 1

            pixel *= self.get_beat()
            return pixel
