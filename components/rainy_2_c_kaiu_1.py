import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def sigmoid(x):
   return 1 / (1 + np.exp( -x ) )

def smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y


def smoothstep(x, th, smooth):
    # return sigmoid((x - th) / smooth)
    return smoothstep_zero(x - th, smooth)


def step(x, th):
    return (x > th).astype(float)


def rect(shape, pos, r, smooth):
    if smooth > 0:
        s = lambda x, th: smoothstep(x, th, smooth)
    else:
        s = step

    ii, jj = np.indices(shape)
    y, x = pos
    horizontal = 1 - s(np.abs(y - ii), r)
    vertical = 1 - s(np.abs(x - jj), r)
    return horizontal * vertical

# def ichimatsu(shape, pos, phase):
#     ii, jj = np.indices(shape)
#     ys = ii - pos[0]
#     xs = jj - pos[1]

#     return np.cos((xs + ys + phase) * np.pi) / 2 + 1

def ichimatsu(shape, pos, phase):
    ii, jj = np.indices(shape)
    ys = ii - pos[0]
    xs = jj - pos[1]

    return ((xs + ys) % 2 == 0).astype(float)


class Rainy2CKaiu1():
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit():
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = (np.random.random(2) * np.array([HEIGHT, WIDTH])).astype(int)
            self.counter = 0
            self.val = 1
            self.active = True
            self.r = 1

        def update(self):
            self.counter += 1
            # !param key: :decay, min: 0, max: 1.0, value: 0.067
            decay = params.fget(__file__, 'decay')
            self.val -= decay
            # !param key: :speed, min: 0, max: 1.0, value: 0.425
            speed = params.fget(__file__, 'speed')
            self.r += speed
            if self.val <= 0:
                self.active = False

        def draw(self):
            # !param key: :smooth, min: 0, max: 1.0, value: 0.05
            smooth = params.fget(__file__, 'smooth')

            pixel = np.zeros(self.shape)
            pixel += ichimatsu(pixel.shape, self.pos, 0) * rect(pixel.shape, self.pos, self.r, smooth) * self.val
            return pixel

