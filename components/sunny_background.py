import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger
from args import args


class SunnyBackground:
    # ガウス分布で虹を描く
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 3)
        self.counter = 0
        self.units = [self.Unit() for _ in range(4)]
        # self.to_color = self.randomColor()
        # self.reset()

    def update(self):
        for unit in self.units:
            unit.update()

    def draw(self):
        value = np.zeros((HEIGHT, WIDTH))
        for unit in self.units:
            value += unit.draw()

        value[value > 1] = 1
        value[value < -1] = -1

        value = (value + 1) / 2

        hsv = np.zeros(self.shape, dtype=np.float32)
        # !param key: :hsv_h, min: 0, max: 360, value: 296.27
        hsv[:, :, 0] = value * 180 + params.fget(__file__, 'hsv_h')
        # !param key: :hsv_s, min: 0, max: 1, value: 0.40
        if args.zententou:
            hsv[:, :, 1] = 1.0
        else:
            hsv[:, :, 1] = params.fget(__file__, 'hsv_s')
        # !param key: :hsv_v, min: 0, max: 1, value: 1
        hsv[:, :, 2] = params.fget(__file__, 'hsv_v')


        pixel = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        return pixel

    class Unit:
        # ガウス分布の中心点
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = np.random.random(2)
            a = np.random.random() * np.pi * 2
            self.v = np.array([np.cos(a), np.sin(a)]) * 0.01
            self.sign = np.random.randint(2) * 2 - 1

        def update(self):
            self.pos += self.v
            y, x = self.pos
            vy, vx = self.v

            if x < 0 and vx < 0 or x > 1 and vx > 0:
                vx *= -1

            if y < 0 and vy < 0 or y > 1 and vy > 0:
                vy *= -1

            self.v = np.array([vy, vx])

        def draw(self):
            pixel = np.zeros(self.shape)
            ii, jj = np.indices(self.shape)
            ys = ii / HEIGHT
            xs = jj / WIDTH
            dxs = xs - self.pos[1]
            # アスペクト比を補正する
            dys = (ys - self.pos[0]) * HEIGHT/WIDTH
            # !param key: :sigma, min: 0, max: 1, value: 0.2
            sigma = params.fget(__file__, 'sigma')
            val = (np.exp(-(dxs ** 2 + dys ** 2) / (2 * sigma ** 2))) / np.sqrt(2*np.pi)
            pixel[ii, jj] = val
            return pixel * self.sign
