import requests
import json
import time
import threading
import atexit

import cv2
import numpy as np

from params import params

import osc.sensor
import osc.wet
from datetime import datetime

from args import args

class OpenWeatherMap:
    def __init__(self):
        self.weather_id = 800
        self.to_close = False

        self.thread = threading.Thread(target = self.run)
        self.thread.start()

        self.wind = 0
        self.rain = 0
        self.data = None

        # self.run()

    def run(self):
        while True:
            self.update()
            time.sleep(2)

            if self.to_close:
                break

        print('open weather map finished')

    def is_sunny(self):
        return self.weather_id // 100 == 8 and self.weather_id <= 803

    def is_cloudy(self):
        return self.weather_id == 804

    def is_rainy(self):
        return not ( self.is_sunny() or self.is_cloudy() )

    def update(self):
        key = '8dc5118d39520fdd511ce26d92e59d1f'
        api = "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}"
        city_name = 'Shimbashi, JP'
        url = api.format(city = city_name, key = key)
        response = requests.get(url)
        data = json.loads(response.text)

        self.weather_id = data['weather'][0]['id']
        self.wind = data['wind']['speed']

    def close(self):
        self.to_close = True


class Weather:
    def __init__(self):
        self.open_weather_map = OpenWeatherMap()
        self.weather = 'sunny'
        self.rain = 1
        self.wind = 0
        self.special = None
        self.counter = 0

        self.use_sensor = True
        # self.end_rain_timer = time.time()

    def update(self):
        if not args.debug:
            self.update_rain()
            self.update_wind()
            self.update_weather()
        self.update_wind_color()

        if self.counter % 30 == 0:
            print(datetime.now(), self.weather, self.rain, self.wind, file=open('log/weather.log', 'a'))

        self.counter += 1

    def update_rain(self):
        rain = osc.sensor.server.rain_rate
        wet = osc.wet.server.wet
        min_rain = 0

        if self.use_sensor:
            if self.open_weather_map.is_rainy():
                # if rain <= min_rain:
                #     self.rain = 0
                #     # if self.weather == 'rainy':
                #     #     self.weather = 'cloudy'
                #         # if time.time() - self.end_rain_timer > 30.0:
                # else:
                rain = max(min_rain, rain)
                self.rain = int((rain - min_rain) / 30 * 10) + 1
                if self.rain > 9:
                    self.rain = 9
            else:
                self.rain = 0
        else:
            if self.open_weather_map.is_rainy():
                weather_id = self.open_weather_map.weather_id

                if weather_id // 100 == 3:
                    self.rain = 9
                elif weather_id // 100 == 5:
                    id_001 = weather_id % 10
                    if id_001 == 0:
                        self.rain = 1
                    elif id_001 == 1:
                        self.rain = 3
                    elif id_001 == 2:
                        self.rain = 5
                    elif id_001 == 3:
                        self.rain = 7
                    elif id_001 == 4:
                        self.rain = 9
                    else:
                        self.rain = 1
                else:
                    self.rain = 1

            else:
                self.rain = 0

        # if wet <= 0.1:
        #     self.rain = 0
        # else:
        #     # if rain <= min_rain:
        #     #     self.rain = 0
        #     #     # if self.weather == 'rainy':
        #     #     #     self.weather = 'cloudy'
        #     #         # if time.time() - self.end_rain_timer > 30.0:
        #     # else:

        #     rain = max(min_rain, rain)
        #     self.rain = (rain - min_rain) // 20 + 1
        #     if self.rain > 9:
        #         self.rain = 9

    def update_weather(self):
        # if self.rain > 0:
        if self.open_weather_map.is_rainy() and osc.wet.server.mean_wet() > 0:
            self.weather = 'rainy'
        else:
            if self.open_weather_map.is_sunny():
                self.weather = 'sunny'
            else:
                self.weather = 'cloudy'

    # def weather_openmap_weather(self):
    #     pass
    #     # くもりか晴れ

    def update_wind(self):
        if self.use_sensor:
            wind = osc.sensor.server.wind_speed
            max_wind = 10
            wind = wind / max_wind
            wind = min(1, wind)
            self.wind = wind
        else:
            self.wind = self.open_weather_map.wind

    def update_wind_color(self):
        # key: :hsv_h, min: 0, max: 360, value: 190
        h_min = 135 + 360
        h_max = 263
        hue = h_min + (h_max - h_min) * self.wind
        params.set('RainyBackground2', 'hsv_h', hue)
        if args.zententou:
            params.common.data['color_h'] = hue + 90
            params.common.data['color_s'] = 1
            params.common.data['color_v'] = 1

    def get_wind_color(self):
        h_min = 135 + 360
        h_max = 263
        hue = h_min + (h_max - h_min) * self.wind
        s = 1
        v = 1
        color = cv2.cvtColor(np.array([hue, s, v]).astype(np.float32)[None, None, :], cv2.COLOR_HSV2BGR)
        color = color.reshape(3)
        return color


    def close(self):
        self.open_weather_map.close()


weather = Weather()


@atexit.register
def close():
    print('atexit')
    weather.close()
    return 0


def main():
    pass


if __name__ == '__main__':
    main()
