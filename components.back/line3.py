import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def __smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y

def smoothstep(x, th, smooth):
    return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def istep(x, th=0): # 反転
    return 1 - step(x, th)

def uparea(shape, pos):
    y, x = np.indices(shape)
    b, a = pos
    up = step(x + b - a, y)
    down = step(-x + b + a, y)
    return up * down

def upline(shape, pos):
    direction = np.array([1, 0])
    return uparea(shape, pos + direction/2) - uparea(shape, pos - direction/2)

def downarea(shape, pos):
    y, x = np.indices(shape)
    b, a = pos
    up = istep(x + b - a, y)
    down = istep(-x + b + a, y)
    return up * down

def downline(shape, pos):
    direction = np.array([-1, 0])
    return downarea(shape, pos + direction/2) - downarea(shape, pos - direction/2)


class Line3:
    # LINEシリーズ
    # 上向き三角の波など
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.active = True
            self.length = 10
            self.counter = 0
            self.direction = np.random.choice([-1, 1])
            if self.direction > 0:
                y = 0
            else:
                y = HEIGHT
            self.pos = np.array([y, np.random.randint(-HEIGHT + 1, WIDTH + HEIGHT - 1)]).astype(float)

        def update(self):
            self.pos += np.array([1, 0]) * self.direction
            self.counter += 1

        def draw(self):
            if self.direction > 0:
                pixel = upline(self.shape, self.pos)
            else:
                pixel = downline(self.shape, self.pos)

            if self.counter > 20 and (pixel == 0).all():
                self.active = False
            return pixel

