import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class Rain():
    # 水面の波紋
    def __init__(self):
        shape = (HEIGHT, WIDTH)
        self.pos = np.zeros(shape)
        self.v = np.zeros(shape)

    def update(self):
        if np.random.randint(10) == 0:
            self.pulse()

        k = 0.1
        kh = 0.2 # 水面伝搬係数
        f = 0.1
        a = 0
        a += k * -self.pos # 垂直伝搬

        a += kh * self.propagation(self.pos)

        a += -f * self.v # 元帥
        self.v += a
        self.pos += self.v

    def propagation(self, map):
        # kh: 水平伝搬係数
        f = np.zeros_like(map)
        right = map[:, 1:] - map[:, :-1]
        f[:, :-1] += right
        left  = map[:, :-1] - map[:, 1:]
        f[:, 1:]  += left
        down  = map[1:, :] - map[:-1, :]
        f[:-1, :] += down
        up    = map[:-1, :] - map[1:, :]
        f[1:, :]  += up
        return f

    def pulse(self):
        i = np.random.randint(HEIGHT)
        j = np.random.randint(WIDTH)
        self.v[i, j] = 0.1

    def draw(self):
        value = self.pos
        hue = np.ones(value.shape) * 194
        satulation = np.ones(value.shape)
        brightness = value + 0.8

        satulation[satulation < 0] = 0
        satulation[satulation > 1] = 1
        brightness[brightness < 0] = 0
        brightness[brightness > 1] = 1

        frame = np.dstack([hue, satulation, brightness]).astype(np.float32)

        frame = cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)
        # frame = np.dstack([frame] * 3)
        return frame


