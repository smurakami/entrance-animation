import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class Noise():
    # ランダムにチカチカするだけ
    def __init__(self):
        self.pixel = np.ones((HEIGHT, WIDTH, 3))

    def update(self):
        self.pixel = np.random.uniform(0, 1, self.pixel.shape)

    def draw(self):
        return self.pixel


