import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def sigmoid(x):
   return 1 / (1 + np.exp( -x ) )

def smoothstep_zero(x, a):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * a)) / 2 + 0.5
    y[x < -a] = 0
    y[x > a] = 1
    return y

def smoothstep(x, th, a):
    # return sigmoid((x - th) / a)
    return smoothstep_zero(x - th, a)

def step(x, th=0):
    return (x > th).astype(float)


def frac(x):
    return x - np.floor(x)


patterns = np.array([
    [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
    ],
    [
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
    ],
    [
        [0, 0, 0, 0, 0],
        [0, 1, 0, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 0, 1, 0],
        [0, 0, 0, 0, 0],
    ],
    [
        [1, 0, 0, 0, 1],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
        [1, 0, 0, 0, 1],
    ],
])


class Firework():
    # 中くらいの雨のときに出すやつ。
    # 花火的なエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        # color = np.ones((HEIGHT, WIDTH, 3))
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1
        pixel = np.dstack([color, alpha])
        return pixel


    class Unit():
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = (np.random.random(2) * np.array([HEIGHT, WIDTH])).astype(int)
            self.counter = 0
            self.val = 1
            self.active = True
            self.t = 0
            self.smooth = 0

        def update(self):
            self.counter += 1
            self.val -= 0.05
            self.t += 0.5

            if self.val <= 0:
                self.active = False

        def draw(self):
            pixel = np.zeros(self.shape)
            fr = frac(self.t)
            i = int(self.t)
            if i < len(patterns) - 1:
                pattern = patterns[i] * (1 - fr) + patterns[i + 1] * fr
            else:
                pattern = patterns[-1]

            ii, jj = np.indices(pattern.shape)
            def valid(ii, jj, y, x):
                yy = ii + y
                xx = jj + x

                mask =  (yy >= 0) & (yy < HEIGHT) & (xx >= 0) & (xx < WIDTH)
                return ii[mask], jj[mask]

            origin = self.pos - 2
            y, x = origin
            ii, jj = valid(ii, jj, y, x)

            pixel[ii + y, jj + x] = pattern[ii, jj] * self.val

            if self.smooth > 0:
                average_square = (3, 3)
                sigma_x = self.smooth
                pixel = cv2.GaussianBlur(pixel, average_square, sigma_x)
            return pixel

