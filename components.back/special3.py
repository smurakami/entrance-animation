import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y

def smoothstep(x, th, smooth):
    return smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def rightline(shape, pos, length):
    ii, jj = np.indices(shape)
    y, x = pos
    pixel = (1 - step(jj, x)) * step(jj, x - length)
    pixel[ii != y] = 0
    return pixel

def leftline(shape, pos, length):
    ii, jj = np.indices(shape)
    y, x = pos
    pixel = (1 - step(jj, x + length)) * step(jj, x)
    pixel[ii != y] = 0
    return pixel


class Special3:
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        # color = np.ones((HEIGHT, WIDTH, 3))
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.active = True
            self.length = np.random.randint(2, 8)
            self.direction = np.random.choice([-1, 1])
            self.counter = 0
            if self.direction > 0:
                self.x = 0
            else:
                self.x = WIDTH

        def update(self):
            self.x += self.direction
            self.counter += 1

        def draw(self):
            if self.direction > 0:
                line = rightline
                y = 0
            else:
                line = leftline
                y = 1

            pixel = np.zeros(self.shape)
            pos = np.array([y, self.x])
            pixel += line(pixel.shape, pos, self.length)
            pos = np.array([y + 2, self.x])
            pixel += line(pixel.shape, pos, self.length)
            if self.counter > 32 and (pixel == 0).all():
                self.active = False
            return pixel

