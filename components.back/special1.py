import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class Special1:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.value = HEIGHT * WIDTH
        self.is_filling = True
        self.is_waiting = False
        self.wait_counter = 0

    def update(self):
        if self.is_waiting:
            self.wait_counter += 1
            if self.wait_counter > 30:
                self.is_waiting = False
                self.is_filling = not self.is_filling
                self.value = WIDTH * HEIGHT
        else:
            self.value -= 1
            if self.value < 0:
                self.wait_counter = 0
                self.is_waiting = True


    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]

        ii, jj = np.indices((HEIGHT, WIDTH))
        alpha = ii * WIDTH + jj >= self.value
        if not self.is_filling:
            alpha = alpha == False
        alpha = alpha.astype(float)

        pixel = np.dstack([color, alpha])
        return pixel
