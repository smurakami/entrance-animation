import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class ColorFill:
    # 色でだんだん埋まっていく
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 3)
        self.counter = 0
        self.to_color = self.randomColor()
        self.reset()

    def reset(self):
        self.mask = np.zeros(self.shape[:2], bool)
        self.from_color = self.to_color.copy()
        self.to_color = self.randomColor()
        self.values = np.zeros((HEIGHT, WIDTH))
        self.next_counter = 0

    def update(self):
        if self.counter % 3 == 0:
            index = np.where(self.mask == False)
            i, j = index
            if len(i) > 0:
                k = np.random.randint(len(i))
                i, j = i[k], j[k]
                self.mask[i, j] = True

        self.values[self.mask] += 0.1
        self.values[self.values > 1] = 1

        if self.mask.all():
            self.next_counter += 1
            if self.next_counter >= 30:
                self.reset()

        self.counter == 1

    def draw(self):
        from_color = self.from_color[None, None, :]
        to_color = self.to_color[None, None, :]
        values = self.values[:, :, None]

        pixel = from_color + values * (to_color - from_color)

        return pixel

    def randomColor(self):
        return np.array([np.random.random(), np.random.random(), np.random.random()])


# class GaussianRainbow:
#     # ガウス分布で虹を描く
#     def __init__(self):
#         self.shape = (HEIGHT, WIDTH, 3)
#         self.counter = 0
#         self.units = [self.Unit() for _ in range(4)]
#         # self.to_color = self.randomColor()
#         # self.reset()

#     def update(self):
#         for unit in self.units:
#             unit.update()

#     def draw(self):
#         value = np.zeros((HEIGHT, WIDTH))
#         for unit in self.units:
#             value += unit.draw()

#         value[value > 1] = 1
#         value[value < -1] = -1

#         value = (value + 1) / 2

#         hsv = np.zeros(self.shape, dtype=np.float32)
#         hsv[:, :, 0] = value * 180 + 190
#         hsv[:, :, 1] = 1
#         hsv[:, :, 2] = 1

#         pixel = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
#         return pixel

#     class Unit:
#         # ガウス分布の中心点
#         def __init__(self):
#             self.shape = (HEIGHT, WIDTH)
#             self.pos = np.random.random(2)
#             a = np.random.random() * np.pi * 2
#             self.v = np.array([np.cos(a), np.sin(a)]) * 0.01
#             self.sign = np.random.randint(2) * 2 - 1

#         def update(self):
#             self.pos += self.v
#             y, x = self.pos
#             vy, vx = self.v

#             if x < 0 and vx < 0 or x > 1 and vx > 0:
#                 vx *= -1

#             if y < 0 and vy < 0 or y > 1 and vy > 0:
#                 vy *= -1

#             self.v = np.array([vy, vx])

#             norm = np.array([-vx, vy])

#             self.v += norm * 0.01

#         def draw(self):
#             pixel = np.zeros(self.shape)
#             ii, jj = np.indices(self.shape)
#             ys = ii / HEIGHT
#             xs = jj / WIDTH
#             dxs = xs - self.pos[1]
#             # アスペクト比を補正する
#             dys = (ys - self.pos[0]) * HEIGHT/WIDTH
#             sigma = 0.1
#             val = (np.exp(-(dxs ** 2 + dys ** 2) / (2 * sigma ** 2))) / np.sqrt(2*np.pi)
#             pixel[ii, jj] = val
#             return pixel * self.sign


