import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y

def smoothstep(x, th, smooth):
    return smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def line(shape, pos, length):
    ii, jj = np.indices(shape)
    y, x = pos
    pixel = (1 - step(ii, y)) * step(ii, y - length)
    pixel[jj != x] = 0
    return pixel


class Line2:
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.pos = np.array([0, np.random.randint(WIDTH)]).astype(float)
            self.active = True
            self.length = 10

        def update(self):
            self.pos[0] += 1

        def draw(self):
            pixel = np.zeros(self.shape)
            pixel += line(pixel.shape, self.pos, self.length)
            return pixel

