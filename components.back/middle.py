import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger

from .rect_wave_a import RectWaveA
from .rect_wave_b import RectWaveB
from .firework import Firework

from osc.sound import client
import osc


class Middle():
    # 中くらいの雨のときに出すやつ。
    # 3つくらいのエフェクトを混ぜて演出する
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        kinds = [
            RectWaveA.Unit,
            RectWaveB.Unit,
            Firework.Unit
        ]
        Unit = np.random.choice(kinds)
        unit = Unit()
        client.send_pulse(unit.pos.tolist())
        self.units.append(unit)

    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        pixel = np.dstack([color, alpha])
        return pixel
