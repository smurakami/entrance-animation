import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *


class VideoViewer:
    # 映像を読みこんでそのまま流す
    def __init__(self, filepath):
        self.shape = (HEIGHT, WIDTH, 3)
        self.capture = cv2.VideoCapture(filepath)
        self.counter = 0

    def update(self):
        if self.counter >= self.capture.get(cv2.CAP_PROP_FRAME_COUNT) - 1:
            self.counter = 0 #Or whatever as long as it is the same as next line
            self.capture.set(cv2.CAP_PROP_POS_FRAMES, 0)
        ret, frame = self.capture.read()
        self.frame = frame
        self.counter += 1

    def draw(self):
        frame = self.frame
        # 真ん中をトリミングします。
        frame = frame[454:454+172]
        frame = frame[:, 14:]
        frame = scipy.misc.imresize(frame, self.shape[:2], interp='nearest')
        frame = frame / 255.0
        return frame


