import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def __smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y

def smoothstep(x, th, smooth):
    return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def istep(x, th=0): # 反転
    return 1 - step(x, th)

def rightarea(shape, pos):
    y, x = np.indices(shape)
    b, a = pos
    up = istep(x + b - a, y)
    down = step(-x + b + a, y)
    return up * down

def rightline(shape, pos):
    direction = np.array([0, 1])
    return rightarea(shape, pos + direction/2) - rightarea(shape, pos - direction/2)

def leftarea(shape, pos):
    y, x = np.indices(shape)
    b, a = pos
    up = step(x + b - a, y)
    down = istep(-x + b + a, y)
    return up * down

def leftline(shape, pos):
    direction = np.array([0, -1])
    return leftarea(shape, pos + direction/2) - leftarea(shape, pos - direction/2)


class Line4:
    # LINEシリーズ
    # 上向き三角の波など
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.active = True
            self.length = 10
            self.counter = 0
            self.direction = np.random.choice([-1, 1])
            if self.direction > 0:
                x_begin = 0
            else:
                x_begin = WIDTH
            self.pos = np.array([np.random.randint(0, HEIGHT - 1), x_begin])

        def update(self):
            self.pos += np.array([0, 1]) * self.direction
            self.counter += 1

        def draw(self):
            if self.direction > 0:
                pixel = rightline(self.shape, self.pos)
            else:
                pixel = leftline(self.shape, self.pos)

            if self.counter > 20 and (pixel == 0).all():
                self.active = False
            return pixel

