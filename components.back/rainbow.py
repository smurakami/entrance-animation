import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class Rainbow():
    # 虹
    def __init__(self):
        shape = (1, WIDTH)
        self.pos = np.zeros(shape)
        self.v = np.zeros(shape)

    def update(self):
        if np.random.randint(60) == 0:
            self.pulse()

        k = 0.1
        kh = 0.2 # 水面伝搬係数
        f = 0.1
        a = 0
        a += k * -self.pos # 垂直伝搬

        # 水平伝搬のシミュレーション
        right = self.pos[:, 1:] - self.pos[:, :-1]
        a[:, :-1] += kh * right
        left  = self.pos[:, :-1] - self.pos[:, 1:]
        a[:, 1:]  += kh * left
        a += -f * self.v # 元帥
        self.v += a
        self.pos += self.v


    def pulse(self):
        j = np.random.randint(WIDTH * 0.2)
        self.v[0, j] = 0.1

    def draw(self):
        # HSVで変換
        value = self.pos
        hue = (value * 6) / 2 * 360 + 60
        satulation = np.ones(value.shape)
        brightness = np.ones(value.shape)

        satulation[satulation < 0] = 0
        satulation[satulation > 1] = 1
        brightness[brightness < 0] = 0
        brightness[brightness > 1] = 1

        frame = np.dstack([hue, satulation, brightness]).astype(np.float32)
        frame = cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)

        frame = np.vstack([frame] * HEIGHT)

        return frame


