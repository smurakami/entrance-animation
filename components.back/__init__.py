import importlib
import glob
import os
import re


def snail2camel(s):
    return re.sub(r'(^|_)[a-z]', lambda x: x.group(0).upper().replace('_', ''), s)


dirname = os.path.dirname(__file__)

for file in glob.glob(os.path.join(dirname, '*.py')):
    file = os.path.basename(file)
    file = file.replace('.py', '')
    file = os.path.basename(file)
    if file == '__init__':
        continue
    exec('from .{} import {}'.format(file, snail2camel(file)))
