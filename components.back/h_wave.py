import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def __smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y


def smoothstep(x, th, smooth):
    # return sigmoid((x - th) / smooth)
    return __smoothstep_zero(x - th, smooth)


def step(x, th):
    return (x > th).astype(float)


class HWave():
    # 横から見た水面の波紋
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.pos = np.zeros(WIDTH)
        self.v = np.zeros(WIDTH)

    def update(self):
        if np.random.randint(10) == 0:
            self.pulse()

        k = 0.1
        kh = 0.03 # 水平伝搬係数
        f = 0.1
        a = 0
        a += k * -self.pos # 垂直伝搬
        a += kh * self.propagation(self.pos)

        a += -f * self.v # 元帥
        self.v += a
        self.pos += self.v

    def propagation(self, map):
        f = np.zeros_like(map)
        right = map[1:] - map[:-1]
        f[:-1] += right
        left  = map[:-1] - map[1:]
        f[1:]  += left
        return f

    def pulse(self):
        j = np.random.randint(WIDTH)
        self.v[j] = 0.1

    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        ii, jj = np.indices(alpha.shape)

        yy = self.pos[jj] * HEIGHT * 1.5 + 1

        alpha = smoothstep(ii, yy, 0.5)

        pixel = np.dstack([color, alpha])
        return pixel

