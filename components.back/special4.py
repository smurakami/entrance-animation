import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class Special4:
    # Special演出
    # 線でうめていく。
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.value = 0
        self.is_filling = True
        self.is_waiting = False
        self.wait_counter = 0
        self.map = np.zeros((HEIGHT, WIDTH), dtype=int)

    def update(self):
        if self.is_waiting:
            self.wait_counter += 1
            if self.wait_counter > 30:
                self.is_waiting = False
                self.is_filling = not self.is_filling
                self.value = 0
                self.map = np.zeros((HEIGHT, WIDTH), dtype=int)
        else:
            y, x = np.indices((HEIGHT, WIDTH))

            if self.value % 2 == 0:
                cand_y, cand_x = np.where(self.map == 0)
                if len(cand_x) > 0:
                    i = np.random.randint(len(cand_x))
                    y = cand_y[i]
                    x = cand_x[i]
                    self.map[y, x] = 1

            if (self.map == 1).all():
                self.wait_counter = 0
                self.is_waiting = True

            self.value += 1

    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]

        ii, jj = np.indices((HEIGHT, WIDTH))
        alpha = self.map.astype(float)
        if not self.is_filling:
            alpha = alpha == False
        alpha = alpha.astype(float)

        pixel = np.dstack([color, alpha])
        return pixel
