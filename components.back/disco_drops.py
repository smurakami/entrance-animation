import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


class DiscoDrops():
    # アスファルトにポタポタ落ちる雨の動きでディスコ
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.drops = []

    def update(self):
        if np.random.randint(10) == 0:
            self.add()

        self.drops = [drop for drop in self.drops if not drop.to_remove]

        for drop in self.drops:
            drop.update()


    def add(self):
        self.drops.append(self.Unit(color=[np.random.random(), np.random.random(), np.random.random()]))

    def draw(self):
        frame = np.ones(self.shape) * 0.0
        # layers = []
        for drop in self.drops:
            layer = drop.draw()
            alpha = layer[:, :, 3]
            bgr = layer[:, :, :3]
            frame[:, :, :3] = alpha_brend(frame[:, :, :3], bgr, alpha)
            frame[:, :, 3] += alpha

        frame[frame > 1] = 1
        return frame


    class Unit():
        # ポタポタ雨粒のシミュレーション。中で使う。
        def __init__(self, color=[0.0, 0.0, 0.0]):
            self.shape = (HEIGHT, WIDTH, 4)
            self.pos = np.array([np.random.randint(HEIGHT), np.random.randint(WIDTH)])
            self.value = 1
            self.to_remove = False
            self.color = color
            # if disco:
            #     self.color = [np.random.random(), np.random.random(), np.random.random()]
            # else:
            #     # self.color = [0.75] * 3
            #     self.color = [1.0, 0.5, 0]

        def update(self):
            self.value -= 0.01
            if self.value < 0:
                self.to_remove = True

        def draw(self):
            frame = np.zeros(self.shape)
            frame[tuple(self.pos)] =  self.color + [self.value]

            return frame # only

        def drawSide(self, frame):
            # 大きい雨粒
            dirs = [
                [1, 0], [0, 1], [-1, 0], [0, -1],
            ]
            dirs = np.array(dirs)
            def valid(pos):
                return np.all([
                    pos[0] >= 0,
                    pos[0] < HEIGHT,
                    pos[1] >= 0,
                    pos[1] < WIDTH
                    ])
            for dir in dirs:
                pos = self.pos + dir
                if valid(pos):
                    frame[tuple(pos)] = [0, 0, 0, self.value]
