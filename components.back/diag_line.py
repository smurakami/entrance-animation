import cv2
import numpy as np
import scipy.misc
import os
import sys
sys.path.append('..')
from utils import *
from params import *
from trigger import trigger


def __smoothstep_zero(x, smooth):
    y = np.zeros_like(x)
    y = np.sin(x * np.pi / (2 * smooth)) / 2 + 0.5
    y[x < -smooth] = 0
    y[x > smooth] = 1
    return y

def smoothstep(x, th, smooth):
    return __smoothstep_zero(x - th, smooth)

def step(x, th=0):
    return (x > th).astype(float)

def upline(shape, pos, length, smooth=0):
    if smooth > 0:
        s = lambda x: smoothstep(x, 0, smooth)
    else:
        s = step
    width = 1
    w = width + smooth/2
    ii, jj = np.indices(shape)
    y, x = pos
    up = s(jj - x + w * 0.5 / np.sqrt(2) + y - ii) - s(jj - x - w * 0.5 / np.sqrt(2) + y - ii)
    down = s(-(jj - x) + length * 0.5 / np.sqrt(2) + y - ii) - s(-(jj - x) - length * 0.5 / np.sqrt(2) + y - ii)
    return up * down

def downline(shape, pos, length, smooth=0):
    if smooth > 0:
        s = lambda x: smoothstep(x, 0, smooth)
    else:
        s = step
    width = 1
    w = width + smooth/2
    ii, jj = np.indices(shape)
    y, x = pos
    up = s(jj - x + length * 0.5 / np.sqrt(2) + y - ii) - s(jj - x - length * 0.5 / np.sqrt(2) + y - ii)
    down = s(-(jj - x) + w * 0.5 / np.sqrt(2) + y - ii) - s(-(jj - x) - w * 0.5 / np.sqrt(2) + y - ii)
    return up * down

class DiagLine:
    # 中くらいの雨のときに出すやつ。
    # 四角く広がるエフェクト
    def __init__(self):
        self.shape = (HEIGHT, WIDTH, 4)
        self.units = []

    def update(self):
        for unit in self.units:
            unit.update()
        self.units = [unit for unit in self.units if unit.active]

        for i in range(trigger.flush()):
            self.pulse()

    def pulse(self):
        self.units.append(self.Unit())

    def draw(self):
        color = np.ones((HEIGHT, WIDTH, 3)) * params.common.get_bgr()[None, None, :]
        alpha = np.zeros((HEIGHT, WIDTH))

        for unit in self.units:
            alpha += unit.draw()

        alpha[alpha > 1] = 1

        pixel = np.dstack([color, alpha])
        return pixel


    class Unit:
        def __init__(self):
            self.shape = (HEIGHT, WIDTH)
            self.active = True
            self.length = 10
            self.direction = np.random.choice([-1, 1])
            if self.direction > 0:
                y = 0
            else:
                y = HEIGHT
            self.pos = np.array([y, np.random.randint(-HEIGHT + 1, WIDTH + HEIGHT - 1)]).astype(float)

        def update(self):
            self.pos += np.array([self.direction, 1])/np.sqrt(2)

        def draw(self):
            pixel = np.zeros(self.shape)
            if self.direction > 0:
                line = upline
            else:
                line = downline
            pixel += line(pixel.shape, self.pos, self.length, 0)
            if (pixel == 0).all():
                self.active = False
            return pixel

