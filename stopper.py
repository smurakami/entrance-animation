import datetime
from args import args

class Stopper:
    def __init__(self):
        self.is_playing = True
        self.to_stop = False
        self.to_start = False

    def update(self):
        if args.debug:
            return

        prev = self.is_playing

        date = datetime.datetime.now()
        start = datetime.datetime(date.year, date.month, date.day, 6, 0)
        end = datetime.datetime(date.year, date.month, date.day, 18, 30)

        if not self.is_playing and start < date < end:
            self.is_playing = True

        if self.is_playing and (date < start or date > end):
            self.is_playing = False

        if date.weekday() in [5, 6]: # 土日は休もう
            self.is_playing = False

        self.to_start = self.to_stop = True
        if not prev and self.is_playing:
            self.to_start = True
        elif prev and not self.is_playing:
            self.to_stop = True

